//
//  MyGps.swift
//  Social
//
//  Created by Aqib on 11/03/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import GTToast


class MyGps: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource {



    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

    var urlShare:String = "SomeUrl.com"
    var appDel: AppDelegate!
    var tableView: UITableView!
    var allGroups: [group] = []
    var id:String!
    var itemInfo = IndicatorInfo(title: "My Groups")
    var isLoading = false

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return allGroups.count
    }


    func refreshViews()
    {

    }


    func deleteButtonAction(_ sender:UIButton!)
    {


        let num:Int = sender.tag
        let id = self.allGroups[num].group_id!
        let name = self.allGroups[num].group_name!


        let def = UserDefaults.standard
        def.set(id, forKey: "group-id-delete")
        def.set(name, forKey: "group-name-delete")
        def.synchronize()

         NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GroupDelete"), object: nil)

        
//        self.superclass.performSegue(withIdentifier: "DeleteGroup", sender: self)

//        let num:Int = sender.tag
//        let id = "https://jasontask.azurewebsites.net/api/index.php?command=delgroup&groupid=\(self.allGroups[num].group_id!)"
//
//
//
//        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to delete this group?", preferredStyle: UIAlertControllerStyle.alert)
//
//
//        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
//
//
//
//            Alamofire.request(id).responseJSON(completionHandler: { (response) in
//
//                var json =  (response.result.value as! NSDictionary)
//
//                if (json["message"] as! String == "success")
//                {
//                    self.allGroups.remove(at: num)
//                    GTToast.create("Group Deleted!").show()
//                }
//                else{
//                    GTToast.create("Error deleting group").show()
//                }
//
//                self.tableView.reloadData()
//            })
//
//
//        }))
//
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
//
//            alert.dismiss(animated: true, completion: nil)
//        }))
//
//        self.present(alert, animated: true, completion: nil)
//

    }



    func shareButtonAction(_ sender:UIButton!)
    {
        let num:Int = sender.tag
        print("Button tapped\(sender.tag)")
        let textToShare = "You can join my RECOLLEKT group by entering the following code: \(self.allGroups[num].group_code!). If you don't yet have the app, get it here: \(urlShare)"


        let objectsToShare = [textToShare]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

        self.present(activityVC, animated: true, completion: nil)

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell = self.tableView.dequeueReusableCell(withIdentifier: "noteCell") as! NotesTableViewCell

        cell.noteText.text = "\(self.allGroups[indexPath.row].group_name!) (\(self.allGroups[indexPath.row].members_count!))"
        cell.leaveBtn.tag = indexPath.row


        cell.shareBtn.addTarget(self, action: #selector(MyGps.shareButtonAction(_:)), for: UIControlEvents.touchUpInside)

        cell.leaveBtn.addTarget(self, action: #selector(MyGps.deleteButtonAction(_:)), for: UIControlEvents.touchUpInside)

        cell.shareBtn.tag = (indexPath as NSIndexPath).row

        cell.leaveBtn.setImage(UIImage(named: "rubbish-bin.png"), for: .normal)

        cell.leaveBtn.tag = (indexPath as NSIndexPath).row

        cell.leaveBtn.imageView?.contentMode = .scaleAspectFit

        cell.shareBtn.imageView?.contentMode = .scaleAspectFit

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        print ("Parent: \(self.parent?.parent)")
        var gp_id = self.allGroups[indexPath.row].group_id

        
        (self.parent?.parent as! MainVC).segueToGroup(id: gp_id!)

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 60
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.appDel = AppDelegate()
        self.view.backgroundColor = UIColor.black
        let v = GroupsNib.instanceFromNib()
        self.view.addSubview(v)
        self.tableView = v.list

        self.tableView.register(UINib(nibName: "NotesTableViewCell", bundle: nil), forCellReuseIdentifier: "noteCell")


        self.tableView.dataSource = self
        self.tableView.delegate = self

        NotificationCenter.default.addObserver(self, selector: #selector(MyGps.viewDidAppear(_:)), name: NSNotification.Name(rawValue: "MyGroups"), object: nil)

    }

    override func viewDidAppear(_ animated: Bool) {

        if let id = UserDefaults.standard.value(forKey: "id")
        {
            print ("* \(AppDelegate().getTopMostView())")
            if (!self.isLoading && AppDelegate().getTopMostView() == "My Groups")
            {
                self.getAllGroups(id as! String)
                self.isLoading = true

            }

        }

    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getAllGroups(_ id: String)
    {


        self.appDel.showLoading(str: "Loading Groups")
        var apiCall = "http://jasontask.azurewebsites.net/api/index.php?command=loadMyGroup&registar_id="
        apiCall += id

        print(apiCall)

        Alamofire.request(apiCall)
            .responseJSON { response in


                self.allGroups = []
                
                self.isLoading = false

                self.appDel.dissLoading()
                let json  = response.result.value as! NSDictionary
                let result = json["result"]! as! [[String : AnyObject]]

                if (!result.isEmpty)
                {
                    for r in result {
                        let group_id   = r["group_id"]! as! String
                        let created_by = r["created_by"]! as! String
                        let group_name = r["group_name"]! as! String
                        let group_code = r["group_code"]! as! String
                        let members_count = r["members_count"]! as! String
                        let date_created = " "//r["date_created"]! as! String
                        let join_id = " "//r["join_id"]! as! String


                        let gp = group(id: group_id, by: created_by, name: group_name, code:group_code, count:members_count, created:date_created, joined:join_id )

                        self.allGroups.append(gp)

                    }

                    print(self.allGroups)
                    self.tableView.reloadData()
                    //self.performSegueWithIdentifier("joinedGroups", sender: self)

                }
                else
                {
                    GTToast.create("You haven't joined any group").show()
                }
        }


    }

    class group
    {

        var group_id:String!
        var created_by:String!
        var group_name:String!
        var group_code:String!
        var members_count:String!
        var date_created:String!
        var join_id:String!


        init(id: String, by: String, name: String, code:String, count:String, created:String, joined:String)
        {
            self.group_id = id
            self.created_by = by;
            self.group_name = name
            self.group_code = code
            self.members_count = count
            self.date_created = created
            self.join_id = joined

        }


    }


}
