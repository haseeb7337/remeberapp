//
//  LinkedInVC.swift
//  Social
//
//  Created by Aqib on 20/12/2016.
//  Copyright © 2016 root-dev. All rights reserved.
//

import UIKit

class LinkedInVC: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webview: UIWebView!
    var linkedInKey = "81rfapzu3mfh5o"
    var linkedInSecret = "r6MqrDww5M3zd7ft"
    var authorizationEndPoint = "https://www.linkedin.com/uas/oauth2/authorization"
    var accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"


    func startAuthorization() {
        // Specify the response type which should always be "code".
        let responseType = "code"

        // Set the redirect URL. Adding the percent escape characthers is necessary.
        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth"
        //stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())!

        // Create a random string based on the time interval (it will be in the form linkedin12345679).
        let state = "linkedin\(Int(NSDate().timeIntervalSince1970))"

        // Set preferred scope.
        let scope = "r_basicprofile"


        // Create the authorization URL string.
        var authorizationURL = "\(authorizationEndPoint)?"
        authorizationURL += "response_type=\(responseType)&"
        authorizationURL += "client_id=\(linkedInKey)&"
        authorizationURL += "redirect_uri=\(redirectURL)&"
        authorizationURL += "state=\(state)&"
        authorizationURL += "scope=\(scope)"

        print(authorizationURL)

        // Create a URL request and load it in the web view.
        let request = URLRequest(url: URL(string: authorizationURL)!)


        webview.loadRequest(request)
        
        
    }


    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.url!
        print(url)

        if url.host == "com.appcoda.linkedin.oauth" {
            if url.absoluteString.range(of: "code") != nil {

                // Extract the authorization code.
                let urlParts = url.absoluteString.components(separatedBy: "?")

                let code = urlParts[1].components(separatedBy: "=")[1]

                requestForAccessToken(authorizationCode: code)
            }
        }
        
        return true
    }
    func requestForAccessToken(authorizationCode: String) {
        let grantType = "authorization_code"

        let redirectURL = "https://com.appcoda.linkedin.oauth/oauth".addingPercentEncoding(withAllowedCharacters: .alphanumerics)

        //stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.alphanumericCharacterSet())!


        // Set the POST parameters.
        var postParams = "grant_type=\(grantType)&"
        postParams += "code=\(authorizationCode)&"
        postParams += "redirect_uri=\(redirectURL)&"
        postParams += "client_id=\(linkedInKey)&"
        postParams += "client_secret=\(linkedInSecret)"


        let postData = postParams.data(using: String.Encoding.utf8)
        var request = URLRequest(url: URL(string: accessTokenEndPoint)!)

        // Indicate that we're about to make a POST request.
        request.httpMethod = "POST"

        // Set the HTTP body using the postData object created above.
        request.httpBody = postData


        // Add the required HTTP header field.
        request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")


        // Initialize a NSURLSession object.
        let session = URLSession(configuration: URLSessionConfiguration.default)

        // Make the request.
        let task: URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in


            // Get the HTTP status code of the request.
            let statusCode = (response as! HTTPURLResponse).statusCode

            if statusCode == 200 {
                // Convert the received JSON data into a dictionary.
                do {
                    let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary

                    let accessToken = dataDictionary["access_token"] as! String

                    UserDefaults.standard.set(accessToken, forKey: "LIAccessToken")
                    UserDefaults.standard.synchronize()

                    DispatchQueue.main.async(execute: { () -> Void in
                        self.dismiss(animated: true, completion: nil)
                    })



                }
                catch {
                    print("Could not convert JSON data into a dictionary.")
                }



            }

        }
        
        task.resume()


        //print (postData)

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        startAuthorization()

        self.webview.delegate = self;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
