//
//  LoginVC.swift
//  Social
//
//  Created by Aqib on 30/01/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import SVProgressHUD
import UIKit
import Alamofire
import FacebookLogin
import FacebookCore
import GTToast
import Contacts
import Photos
import FBSDKCoreKit
import FBSDKLoginKit
import Crashlytics


class LoginVC: UIViewController, LoginButtonDelegate {

    @IBOutlet weak var email: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var password: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var fbLoginView: UIView!
    var contactsStore:CNContactStore!

    var loginButton:LoginButton!


    var id:String = "id"
    var registrar_id:String = "registrar_id"
    var name:String = "username"
    var emailS:String = "email"
    var about:String = "bio"
    var picture:String = "picture"
    var signup_method = "method"
    let url = "https://jasontask.azurewebsites.net"

    var usernameString:String?
    var passwordString:String?

    let lightGreyColor = UIColor(red: 197/255, green: 205/255, blue: 205/255, alpha: 1.0)
    let darkGreyColor = UIColor(red: 52/255, green: 42/255, blue: 61/255, alpha: 1.0)
    let overcastBlueColor = UIColor(red: 0, green: 187/255, blue: 204/255, alpha: 1.0)

    func applySkyscannerThemeWithIcon(textField: SkyFloatingLabelTextFieldWithIcon) {
        self.applySkyscannerTheme(textField: textField)

        textField.iconColor = UIColor.white
        textField.selectedIconColor = UIColor.white
        var font = UIFont(name: "FontAwesome", size: 17)
        print ("My Font: \(font)")
        textField.iconFont = font



    }

    func applySkyscannerTheme(textField: SkyFloatingLabelTextField) {

        textField.tintColor = UIColor.white

        textField.textColor = UIColor.white
        textField.lineColor = UIColor.white

        textField.selectedTitleColor = UIColor.white
        textField.selectedLineColor = UIColor.white


    }

    func forceCrash()
    {
            Crashlytics.sharedInstance().crash()



    }

    override func viewDidLoad() {


        //forceCrash()

        self.applySkyscannerThemeWithIcon(textField: self.email)
        self.applySkyscannerThemeWithIcon(textField: self.password)

        self.password.isSecureTextEntry = true


        //self.email.iconFont = UIFont(name: "FontAwesome", size: 15)
        self.email.iconText = "\u{f007}"


        //self.password.iconFont = UIFont(name: "FontAwesome", size: 15)
        self.password.iconText = "\u{f023}"


        self.contactsStore = CNContactStore()

        let status:CNAuthorizationStatus = CNContactStore.authorizationStatus(for: .contacts)

        if status == CNAuthorizationStatus.notDetermined
        {
            contactsStore.requestAccess(for: .contacts, completionHandler: { (bb, err) in


            })
        }

        PHPhotoLibrary.requestAuthorization { (status) -> Void in

            switch status{
            case .authorized:

                break
            case .denied:

                break
            default:

                break
            }
        }


        super.viewDidLoad()



        //self.initialY = self.view.frame.origin.y


        if let accessToken = AccessToken.current {
            // User is logged in, use 'accessToken' here.

            print ("Logged In")

        }
        else{
            print ("NOT Logged In")
        }


        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)


        //            self.fbLoginButton.readPermissions = ["public_profile","email", "user_friends", "user_about_me"]
        //            self.fbLoginButton.delegate = self


        loginButton = LoginButton(readPermissions: [ .publicProfile, .email, .userFriends ])
        
        loginButton.center = self.fbLoginView.center
        
        //loginButton.loginBehavior = .web
        
        loginButton.delegate = self
        
        
        self.fbLoginView.addSubview(loginButton)
    }


    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    @IBAction func forgotPassword(_ sender: UIButton) {

        if (self.email.text != "")
        {

            var apiCall = "http://jasontask.azurewebsites.net/api/index.php?command=forgotpass&user_email=\(self.email.text!)"


            AppDelegate().showLoading(str: "Please Wait...")


            Alamofire.request(apiCall)
                .responseJSON { response in
                    debugPrint(response)

                    AppDelegate().showMsg(str:"An Email has been sent to \(self.email.text!)", success: true)


            }
            //Alamofire.request(.post, nil, apiCall).response

        }
        else
        {
            self.view.endEditing(true)
            GTToast.create("Enter Email Address first").show()
        }

    }

    override func viewDidAppear(_ animated: Bool) {

        checkForExistingAccessToken()

        if let accessToken = AccessToken.current {
            // User is logged in, use 'accessToken' here.

            print ("FB Logged In")
        }
        else{
            print ("FB NOT Logged In")
        }




        if let b: AnyObject = UserDefaults.standard.object(forKey: "logout") as AnyObject? {
            if (b as! String == "yes")
            {
                print("logout")
                //self.loadingViewFull.isHidden = true
                let userDefaults = UserDefaults.standard
                userDefaults.removeObject(forKey: "logout")
                userDefaults.removeObject(forKey: "id")
                linkedinLogout()
                userDefaults.synchronize()
                if (FBSDKAccessToken.current() != nil){
                    var logoutBtn:FBSDKLoginManager = FBSDKLoginManager()
                    logoutBtn.logOut()

                }
            }



        }
        else
        {
            print("no logout")
            if let id: AnyObject = UserDefaults.standard.object(forKey: "id") as AnyObject? {


               // GTToast.create("Signing in... Please wait").show()
                print("Already logged in")
                print(id)

                self.registrar_id = id as! String
                checkExistingUserInfo(self.registrar_id)


            }
        }

    }

    @IBAction func emailSignInClicked(_ sender: UIButton) {

        self.usernameString = email.text
        self.passwordString = password.text

        if (email.text != "" && password.text != ""){

            signInAttempt(self.usernameString!, password: self.passwordString!)
        }
        else
        {
            print("Didn't in")
        }


    }

    @IBAction func fbSignInClicked(_ sender: UIButton) {

        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email"))
                {

                    print("login complete")
                    let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath:  "me", parameters:["fields": "id, name, email, picture"])


                    graphRequest.start(completionHandler: { (connection, r, error) -> Void in

                        if ((error) != nil)
                        {
                            // Process error
                            print ("ERROR: \(error)")

                        }
                        else
                        {




                            var result = r as! NSDictionary
                            self.id = (result.value(forKey: "id") as? String)!
                            self.name = (result.value(forKey: "name") as? String)!
                            self.emailS = (result.value(forKey: "email") as? String)!
                            self.about = ""
                            let n = (result.value(forKey: "picture") as? NSDictionary)!
                            let m = (n.value(forKey: "data") as? NSDictionary)!

                            var picUrl = "https://graph.facebook.com/picId/picture?width=9999"
                            self.picture = picUrl.replacingOccurrences(of: "picId", with: self.id)

                            self.signup_method = "facebook"
                            
                            let userDefaults = UserDefaults.standard
                            userDefaults.set(self.id, forKey: "fbId")
                            userDefaults.synchronize()
                            
                            
                            print(self.id)
                            
                            
                            self.checkExistingUserInfo(self.id)
                            
                        }
                    })
                    
                    //        }
                    //        else
                    //        {
                    //            print("Error: \("nooww")")
                    //            print(error.localizedDescription)
                    //        }



                }
            }
        }

    }

    @IBAction func twitterSignInClicked(_ sender: UIButton) {

        var navBar = UINavigationBar.init(frame: CGRect(x:0, y:0, width:self.view.frame.size.width, height: 64))
        navBar.backgroundColor = UIColor.white

        var navItem = UINavigationItem()
        navItem.title = "LinkedIn Login"

        var leftButton = UIBarButtonItem.init(title: "Back", style: .plain, target: self, action: Selector("disView"))

        navItem.leftBarButtonItem = leftButton;


        navBar.items = [navItem]




        if(!LITokenHandler.isValidToken())
        {



            var vc = LIAuthorizationVC.init(successBlock: {
                self.dismiss(animated: true, completion: nil)



                LIRestAPIHandlers.requestProfile({ (dic, response) in

                    var p = dic as! NSDictionary


                    print (p)


                    //var email = p["email"] as! String
                    self.id = "\(p["id"] as! String)"
                    self.name = p["firstName"] as! String
                    self.name = "\(self.name) \(p["lastName"] as! String)"
                    self.picture = "\(p["pictureUrl"] as! String)"
                    self.signup_method = "Linkedin"
                    self.about = ""


                    self.checkExistingUserInfoLinkedin(self.id)
                    //                self.newUserSignup()



                }, failure: { (error) in


                    print (error)
                })



            }, failureBlock: { (err) in


                //GTToast.create(err!.localizedDescription)
            })

            vc!.webViewNavigationNar = navBar
            self.present(vc!, animated: true, completion: nil)
        }
        else
        {

            checkForExistingLinkedInToken()

        }


    }

    @IBAction func SignUpClicked(_ sender: UIButton) {
    }

    func showLoading(str: String)
    {
        SVProgressHUD.show(withStatus: str)
    }

    func dismissLoading()
    {
        SVProgressHUD.dismiss()
    }

    func checkForExistingLinkedInToken()
    {

        if(LITokenHandler.isValidToken())
        {

            LIRestAPIHandlers.requestProfile({ (dic, response) in

                var p = dic as! NSDictionary


                print (p)


                self.id = "\(p["id"] as! String)"
                self.name = p["firstName"] as! String
                self.name = "\(self.name) \(p["lastName"] as! String)"
                self.picture = "\(p["pictureUrl"] as! String)"
                self.signup_method = "Linkedin"
                self.about = ""


                self.newUserSignup()

                
            }, failure: { (error) in

                print (error)
            })

        }
        
    }

    func newUserSignup()
    {

        var url = "https://jasontask.azurewebsites.net/api/index.php?command=add_social_user&"
        url += "registrar_id="
        url += self.id
        url += "&username="
        url += self.name
        url += "&profile_picture="
        url += self.picture
        url += "&signup_method="
        url += self.signup_method
        url += "&user_about="
        url += self.about
        print("----")
        url = url.replacingOccurrences(of: " ", with: "%20")
        print(url)
        print("----")

        Alamofire.request(url).responseJSON { (response) in


            debugPrint(response)

            self.checkExistingUserInfo(self.id)

        }

    }


    func signInAttempt(_ username:String, password:String)
    {

        AppDelegate().showLoading(str: "Please Wait...")
        //GTToast.create("Signing in... Please wait").show()

        ////indicator.startAnimating()
        print("it came in")


        var loginUrl = "https://jasontask.azurewebsites.net/api/index.php?command=login&username=\(username)&password=\(password)"
        loginUrl = loginUrl.replacingOccurrences(of: " ", with: "%20")

        print(loginUrl)

        Alamofire.request(loginUrl)
            .responseJSON { response in
                debugPrint(response)

                //self.//indicator.stopAnimating()
                AppDelegate().dissLoading()
                let json  = response.result.value as! NSDictionary
                let result = json["result"] as? [[String : AnyObject]]


                UIApplication.shared.isNetworkActivityIndicatorVisible = false

                if (!(result == nil))
                {
                    for r in result! {
                        let id = r["id"]! as! String
                        self.registrar_id = r["registrar_id"]! as! String
                        let name = r["username"]! as! String
                        let url = r["profile_pic"] as! String

                        print("account: \(id) \(name)")
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(self.registrar_id, forKey: "id")
                        userDefaults.set(name, forKey: "username")
                        userDefaults.set(url, forKey: "url")

                        userDefaults.synchronize()

                    }
                    self.performSegue(withIdentifier: "loggedin", sender: self)
                }
                else
                {

                    //let json  = (response.result.value! as AnyObject).value("error")
                    //let error = json["error"] as? [[String : AnyObject]]


                    //print("This is the respons: \(json!)")
                    //GTToast.create(json as! String).show()
                    self.password.text = ""
                    
                }
        }
    }
    
    func linkedinLogout()
    {
        if (LITokenHandler.isValidToken())
        {
            LITokenHandler.clearToken()
        }
    }

    func disView()
    {
        self.dismiss(animated: true, completion: nil)
    }

    func getProfileInfo()
    {

        if let accessToken = UserDefaults.standard.object(forKey: "LIAccessToken") {
            // Specify the URL string that we'll get the profile info from.
            let targetURLString = "https://api.linkedin.com/v1/people/~:(public-profile-url)?format=json"

            // Initialize a mutable URL request object.
            var request = URLRequest(url: URL(string: targetURLString)!)

            // Indicate that this is a GET request.
            request.httpMethod = "GET"

            // Add the access token as an HTTP header field.
            request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")


            // Initialize a NSURLSession object.
            let session = URLSession(configuration: URLSessionConfiguration.default)

            // Make the request.
            let task: URLSessionDataTask = session.dataTask(with: request) { (data, response, error) -> Void in

                let statusCode = (response as! HTTPURLResponse).statusCode

                if statusCode == 200 {
                    // Convert the received JSON data into a dictionary.
                    do {
                        let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary

                        let profileURLString = dataDictionary["publicProfileUrl"] as! String


                        DispatchQueue.main.async(execute: { () -> Void in

                            GTToast.create("url: \(profileURLString)").show()

                            // self.btnOpenProfile.setTitle(profileURLString, forState: UIControlState.Normal)
                            //self.btnOpenProfile.hidden = false

                        })
                        
                    }
                    catch {
                        print("Could not convert JSON data into a dictionary.")
                    }
                }
                
            }
            
            task.resume()
            
        }
        
    }

    func checkForExistingAccessToken() {
        if UserDefaults.standard.object(forKey: "LIAccessToken") != nil {
            btnSignIn.isEnabled = false
            //btnGetProfileInfo.enabled = true

            getProfileInfo()


        }
    }


    func checkExistingUserInfo(_ id: String)
    {

        //indicator.startAnimating()
        //indicator.color = UIColor.red
        //loadingViewFull.isHidden = false

        self.showLoading(str: "Please wait...")

        print("checking existing user information...")
        var checkurl = "https://jasontask.azurewebsites.net/api/index.php?command=check_registrar&registrar_id="
        checkurl += id


        Alamofire.request(checkurl)
            .responseJSON { response in

                AppDelegate().showLoading(str: "Please wait...")

                debugPrint(response)

                //self.indicator.stopAnimating()

                if (response.result.value as? NSDictionary != nil)
                {


                    let json  = response.result.value as! NSDictionary
                    let result = json["result"]! as! [[String : AnyObject]]

                    if (!result.isEmpty)
                    {
                        for r in result {
                            let id = r["id"]! as! String
                            self.registrar_id = r["registrar_id"]! as! String
                            let name = r["username"]! as! String
                            let url = r["profile_pic"] as! String

                            print("account: \(id) \(name)")
                            let userDefaults = UserDefaults.standard
                            userDefaults.set(self.registrar_id, forKey: "id")
                            userDefaults.set(name, forKey: "username")
                            userDefaults.set(url, forKey: "url")

                            userDefaults.synchronize()
                            AppDelegate().dissLoading()
                        }
                        self.performSegue(withIdentifier: "loggedin", sender: self)
                    }
                    else
                    {
                        print("not a user already...")
                        print("registering...")
                        
                        self.newUserSignup()
                    }

                    
                }


        }

    }


    func checkExistingUserInfoLinkedin(_ id: String)
    {

        ////indicator.startAnimating()

        //loadingViewFull.isHidden = false

        self.showLoading(str: "Please wait...")

        print("checking existing user information...")
        var checkurl = "https://jasontask.azurewebsites.net/api/index.php?command=check_registrar&registrar_id="
        checkurl += id


        Alamofire.request(checkurl)
            .responseJSON { response in

                AppDelegate().showLoading(str: "Please wait...")

                debugPrint(response)

                //self.indicator.stopAnimating()
                let json  = response.result.value as! NSDictionary
                let result = json["result"]! as! [[String : AnyObject]]

                if (!result.isEmpty)
                {
                    for r in result {
                        let id = r["id"]! as! String
                        self.registrar_id = r["registrar_id"]! as! String
                        let name = r["username"]! as! String
                        let url = r["profile_pic"] as! String

                        print("account: \(id) \(name)")
                        let userDefaults = UserDefaults.standard
                        userDefaults.set(self.registrar_id, forKey: "id")
                        userDefaults.set(name, forKey: "username")
                        userDefaults.set(url, forKey: "url")

                        userDefaults.synchronize()
                        AppDelegate().dissLoading()
                    }
                    self.performSegue(withIdentifier: "loggedin", sender: self)
                }
                else
                {
                    print("not a user already...")
                    print("registering...")

                    //self.checkExistingUserInfoLinkedin(self.id)
                    self.newUserSignup()
                }
        }

    }

    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {


        //        if (result.access == nil)
        //        {
        print("login complete")
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath:  "me", parameters:["fields": "id, name, email, picture"])


        graphRequest.start(completionHandler: { (connection, r, error) -> Void in

            if ((error) != nil)
            {
                // Process error
                print ("ERROR: \(error)")

            }
            else
            {




                var result = r as! NSDictionary
                self.id = (result.value(forKey: "id") as? String)!
                self.name = (result.value(forKey: "name") as? String)!
                self.emailS = (result.value(forKey: "email") as? String)!
                self.about = ""
                let n = (result.value(forKey: "picture") as? NSDictionary)!
                let m = (n.value(forKey: "data") as? NSDictionary)!

                var picUrl = "https://graph.facebook.com/picId/picture?width=9999"
                self.picture = picUrl.replacingOccurrences(of: "picId", with: self.id)

                self.signup_method = "facebook"

                let userDefaults = UserDefaults.standard
                userDefaults.set(self.id, forKey: "fbId")
                userDefaults.synchronize()


                print(self.id)


                self.checkExistingUserInfo(self.id)

            }
        })
        
        //        }
        //        else
        //        {
        //            print("Error: \("nooww")")
        //            print(error.localizedDescription)
        //        }
        
        
        
    }

    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {


        if (error == nil)
        {
            print("login complete")
            let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath:  "me", parameters:["fields": "id, name, email, bio, picture"])


            graphRequest.start(completionHandler: { (connection, r, error) -> Void in

                if ((error) != nil)
                {
                    // Process error
                    print ("ERROR: \(error)")

                }
                else
                {


                    var result = r as! NSDictionary
                    self.id = (result.value(forKey: "id") as? String)!
                    self.name = (result.value(forKey: "name") as? String)!
                    self.emailS = (result.value(forKey: "email") as? String)!
                    self.about = (result.value(forKey: "bio") as? String)!
                    let n = (result.value(forKey: "picture") as? NSDictionary)!
                    let m = (n.value(forKey: "data") as? NSDictionary)!
                    var picUrl = "https://graph.facebook.com/id/picture?width=9999"
                    self.picture = picUrl.replacingOccurrences(of: "id", with: self.id)
                    self.signup_method = "facebook"

                    let userDefaults = UserDefaults.standard
                    userDefaults.set(self.id, forKey: "fbId")
                    userDefaults.synchronize()


                    print(self.id)


                    self.checkExistingUserInfo(self.id)

                }
            })

        }
        else
        {
            print("Error: \("nooww")")
            print(error.localizedDescription)
        }



    }

    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        print("user logged out")
        //loadingViewFull.isHidden = true
        self.dismissLoading();
    }




}
