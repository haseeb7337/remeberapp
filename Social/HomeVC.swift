//
//  HomeVC.swift
//  Social
//
//  Created by Aqib on 09/03/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import UIKit
import Alamofire

class HomeVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var im:UIImage!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var username: UILabel!
    var userid: String!

    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.userid = AppDelegate().getUserId()

        NotificationCenter.default.addObserver(self, selector: #selector(changePhoto), name: NSNotification.Name(rawValue: "ChangeProfilePhoto"), object: nil)

        if let url = UserDefaults.standard.value(forKey: "url")
        {
            Alamofire.request(url as! String).responseImage { response in

                self.profilePic.image = response.result.value!
            }

        }

        if let name = UserDefaults.standard.value(forKey: "username")
        {
            self.username.text = (name as! String)
            
        }





        // Do any additional setup after loading the view.
    }


    func changePhoto()
    {
        print("Profile Tapped")
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Button capture")


            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            imagePicker.allowsEditing = true

            self.present(imagePicker, animated: true, completion: nil)
        }

    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {

            self.im = image

        } else{
            print("Something went wrong")
        }

        //self.dismiss(animated: true, completion: nil)

        myImageUploadRequest()

    }



    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }

    func myImageUploadRequest()
    {

        AppDelegate().showLoading(str: "Uploading Image...")

        let myUrl:URL!
        myUrl = URL(string: "http://jasontask.azurewebsites.net/api/index.php?command=udpateprofilepic&user_id=\(self.userid!)")!;

        var request = URLRequest(url:myUrl!);
        request.httpMethod = "POST";


        //print("page number= \(page)")

        //var currentNumber = Int(page)


        let param = [
            "userid"  : "\(self.userid!)"
        ]



        print("myUrl: \(myUrl) params: userid=\(self.userid!)");


        let boundary = generateBoundaryString()

        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")


        let imageData = UIImageJPEGRepresentation(im, 0)

        if(imageData==nil)  { return; }

        request.httpBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData!, boundary: boundary)



        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            data, response, error in

            AppDelegate().dissLoading()

            if error != nil {

                print("error=\(error)")

                AppDelegate().showMsg(str: "Error", success: false)
                return
            }
            else{
                AppDelegate().showMsg(str: "Contact created", success: true)

            }


            // You can print out response object
            print("******* response = \(response)")

            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")

            var json = self.convertToDictionary(text: responseString as! String)

            var ms = json!["link"] as! String


            let userDefaults = UserDefaults.standard
            userDefaults.set(ms, forKey: "url")
            userDefaults.synchronize()



            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "profilePicUpdated"), object: nil)

            DispatchQueue.main.async {
                
                self.imagePicker.dismiss(animated: false, completion: nil)

                //profilePicUpdated
                self.profilePic.image = self.im
            }
            
            
        }) 
        
        task.resume()
        
    }

    func randomStringWithLength(length: Int) -> String {

        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }

        return randomString
    }


    func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        let body = NSMutableData();

        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }


        let filename = "user-image-\(randomStringWithLength(length: 15)).jpg"

        let mimetype = "image/jpg"

        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")



        body.appendString("--\(boundary)--\r\n")

        return body as Data
    }

    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func createGroupClicked(_ sender: UIButton) {

        (self.parent as! MainVC).segueToCreateGroup()
    }

    @IBAction func joinGroupsClicked(_ sender: UIButton) {

        (self.parent as! MainVC).segueToJoinGroups()
    }


    @IBAction func myGroupsClicked(_ sender: UIButton) {

        (self.parent as! MainVC).segueToMyGroups()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
