//
//  MainVC.swift
//  Social
//
//  Created by Aqib on 30/01/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import UIKit
import KYDrawerController
import FBSDKLoginKit

class MainVC: UIViewController {

    @IBOutlet weak var barIcon: UIBarButtonItem!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    var drawer:KYDrawerController!
    var im:UIImage!


    var segueString:String = ""


    @IBOutlet weak var menuBarButton: UIBarButtonItem!
    @IBOutlet weak var groupView: UIView!
    @IBOutlet weak var home: UIView!
    @IBOutlet weak var profile: UIView!
    @IBOutlet weak var createGp: UIView!
    @IBOutlet weak var invitationcodes: UIView!
    @IBOutlet weak var myGroups: UIView!
    @IBOutlet weak var joinGroup: UIView!
    @IBOutlet weak var notifications: UIView!


    var imagePicker = UIImagePickerController()

    var homeOptions:[String] = ["Change Picture "]
    //var homeOptionsImg:[String] =
    var groupOptions:[String] = ["ADD CONTACT", "Address Book", "New Contact","Social Network","THIS CONTACT", "Change Picture"]
    var groupOptionsImg:[String] = ["", "adbk", "phn","" ,"", "cmr"]

    var groupOptionsNP:[String] = ["ADD CONTACT", "Address Book", "New Contact","Socail Network"]
    var groupOptionsImgNP:[String] = ["", "adbk", "phn", ""]


    var joinGpOptions:[String] = ["Join Group", "Create Group"]
    var myGpOptions:[String] = ["Create Group", "Join Group"]








    override func viewDidAppear(_ animated: Bool) {


        if let b = UserDefaults.standard.string(forKey: "selectedDrawer") {

            if (b == "My Profile")
            {
                segueToProfile()
            }


            if (b == "Invitation Code")
            {
                segueToInvitation()
            }


            if (b == "My Groups")
            {
                segueToMyGroups()
            }


            if (b == "Join Groups")
            {
                segueToJoinGroups()
            }


            if (b == "Notifications")
            {
                segueToNotifications()
            }

            if (b == "Sign Out")
            {
                logout()
            }

            let u = UserDefaults.standard
            u.removeObject(forKey: "selectedDrawer")
            u.synchronize()


        }
        else{
            print("nil found")
            //segueToHome()
            //segueToProfile()
        }

    }

    func hideMenu()
    {
        self.view.endEditing(true)
        menuBtn.isEnabled = false
    }

    func showMenuV()
    {
        self.view.endEditing(true)
        menuBtn.isEnabled = true
    }

    @IBAction func showMenu(_ sender: UIBarButtonItem) {

        self.view.endEditing(true)

        self.view.endEditing(true)
        var hh = self.navigationController!.navigationBar.frame.size.height + 5

        var vv = UIView(frame: CGRect(x: self.view.frame.maxX - 30, y: hh, width: 20, height: 20))

        var menu: [String]!

        var imgMenu:[String] = []

        if let b = UserDefaults.standard.string(forKey: "top") {

            print("OnTop", b as! String)

            switch String((b as! String).characters.split(separator: ",")[0])
            {
            case "My Profile":
                menu = self.homeOptions
                break;

            case "My Groups":
                menu = self.myGpOptions
                break;

            case "Join Groups":
                menu = self.joinGpOptions
                break;

            case "Group View":
                menu = groupOptions
                imgMenu = groupOptionsImg
                break;

            case "Group ViewNP":
                menu = groupOptionsNP
                imgMenu = groupOptionsImgNP
                break;
                
            case "CreateJoinGroup":
                menu = groupOptions
                break;

            default:
                menu = self.homeOptions

            }


            FTPopOverMenu.showForSender(
            sender: vv,
             with: menu,
             menuImageArray: imgMenu,
             done: { (selectedIndex) -> () in

                self.view.endEditing(true)

                switch (menu[selectedIndex])
                {

                case "Change Picture":
                    //ChangePhoto
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangePhoto"), object: nil)
                    break;

                case "Change Picture ":
                    //ChangePhoto
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeProfilePhoto"), object: nil)
                    break;

                case "Create Group":
                    self.segueToCreateGroup()
                    self.hideMenu()
                    break;

                case "Join Group":
                    self.segueToJoinGroups()
                    self.hideMenu()
                    break;
                case "Address Book":
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addBook"), object: nil)
                    break;

                case "New Contact":
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newContact"), object: nil)
                    break;

                case "Social Network":
                    self.performSegue(withIdentifier: "socialNewContact", sender: self)
                    break;


                default:
                    print (menu[selectedIndex])
                }

            }) {

            }

        }




    }

    override func viewDidLoad() {
        super.viewDidLoad()

        drawer = self.navigationController!.parent as! KYDrawerController!

        print("**Loaded")

        NotificationCenter.default.addObserver(self, selector: #selector(self.hideMenu), name: NSNotification.Name(rawValue: "HideMenu"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.showMenuV), name: NSNotification.Name(rawValue: "showMenu"), object: nil)

        segueToProfile()



        // Do any additional setup after loading the view.
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func RButtonPressed(_ sender: UIBarButtonItem) {
        self.view.endEditing(true)
        self.segueToProfile()
    }

    @IBAction func sliderToggle(_ sender: UIBarButtonItem) {

       self.view.endEditing(true)
        drawer.setDrawerState(.opened, animated: true)
        sender.tag = 1

    }

    func segueToCreateGroup()
    {
        AppDelegate().setTopMostView(top: "CreateJoinGroup")
        self.view.bringSubview(toFront: createGp)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CreateJoinGroup"), object: nil)
        hideMenu()
        // self.performSegue(withIdentifier: "Home", sender: self)
    }

    func segueToProfile()
    {
        AppDelegate().setTopMostView(top: "My Profile")
        self.view.bringSubview(toFront: home)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MyProfile"), object: nil)
        showMenuV()
        // self.performSegue(withIdentifier: "Home", sender: self)
    }

    func segueToInvitation()
    {
        AppDelegate().setTopMostView(top: "Invitation Code")
        self.view.bringSubview(toFront: invitationcodes)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "InvitationCode"), object: nil)
        hideMenu()
        // self.performSegue(withIdentifier: "Home", sender: self)
    }

    func segueToMyGroups()
    {

        AppDelegate().setTopMostView(top: "My Groups")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MyGroups"), object: nil)

        self.view.bringSubview(toFront: myGroups)
        showMenuV()
        // self.performSegue(withIdentifier: "Home", sender: self)

    }

    func segueToJoinGroups()
    {
        AppDelegate().setTopMostView(top: "CreateJoinGroup")
        self.view.bringSubview(toFront: joinGroup)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CreateJoinGroup"), object: nil)
        hideMenu()
        // self.performSegue(withIdentifier: "Home", sender: self)
    }

    func segueToNotifications()
    {
        AppDelegate().setTopMostView(top: "Notifications")
        self.view.bringSubview(toFront: notifications)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Notifications"), object: nil)
        hideMenu()
        // self.performSegue(withIdentifier: "Home", sender: self)
    }

    func segueToGroup(id: String)
    {
        AppDelegate().setTopMostView(top: "Group View,\(id)")

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GroupView"), object: nil)
        self.view.bringSubview(toFront: groupView)
        showMenuV()
        
        
    }

    func segueToSocial(id: String)
    {
        AppDelegate().setTopMostView(top: "Social Media,\(id)")

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SocialMedia"), object: nil)
        self.view.bringSubview(toFront: groupView)
        hideMenu()
        
        
    }

    func backButton(v: UIView)
    {
        self.view.sendSubview(toBack: v)
        //print(v)
    }

    func topMostView()
    {
        print (self.view.subviews.first)
        print (self.view.subviews.last)
    }

    func logout()
    {

        if (FBSDKAccessToken.current != nil)
        {
            self.fbLogout()
        }
        self.clearPrefs()
        self.dismiss(animated: true)
    }


    func fbLogout()
    {
        let loginManager = FBSDKLoginManager()
        loginManager.logOut() // this is an instance function

    }

    func clearPrefs()
    {
        let userDefaults = UserDefaults.standard
        userDefaults.set("yes", forKey: "logout")
        userDefaults.synchronize()
    }
    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
