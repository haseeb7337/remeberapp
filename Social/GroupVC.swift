//
//  GroupVC.swift
//  Social
//
//  Created by Aqib on 13/03/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//


import UIKit
import GTToast
import Alamofire
import AlamofireImage
import Foundation
import SCLAlertView

extension String {
    var length: Int { return characters.count    }  // Swift 2.0
}


extension UIView {
    class func loadFromNibNamed(_ nibNamed: String, bundle : Bundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiate(withOwner: nil, options: nil)[0] as? UIView
    }
}

class GroupVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate , UITextViewDelegate, UIScrollViewDelegate {

    var allImageViews:[UIImageView] = []

    @IBOutlet weak var pgControl: UIPageControl!
    var groupCode: String!
    @IBOutlet weak var scrollview: UIScrollView!
    var allContacts: [contact] = []
    var userid:String!
    var count:Int = 0;
    var currentItemNumber = 0
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var note: UITextView!
    var prev:Int=0
    var imagePicker = UIImagePickerController()

    var im:UIImage!

    @IBAction func refresh(_ sender: UIButton) {
        self.viewDidAppear(true)
    }

    func setViewsForContact(n:Int)
    {

        self.pgControl.currentPage = n

        self.note.isUserInteractionEnabled = true

        if (self.allImageViews[n].image == nil)
        {
            Alamofire.request(allContacts[n].picture).responseImage { response in

                print("Loading")

                print(response.request)

                if let image = response.result.value {
                    self.allImageViews[n].image = image
                }
            }
        }
        else
        {
                print ("Already Loaded")
        }

        self.pgControl.updateCurrentPageDisplay()
        self.contactName.text = self.allContacts[n].name
        self.note.text = self.allContacts[n].notes

        UIView.animate(withDuration: 0.5, animations: {
            self.contactName.alpha = 1.0
            self.note.alpha = 1.0

            //self.imageBtn.alpha = 1.0
        })
    }



    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        if (!self.note.isHidden)
        {
            editTapped(self.note.text!)
        }
    }



    func textViewDidBeginEditing(_ textView: UITextView) {

        if (textView.text! == "No Notes Added"){

            textView.text = ""
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {

        if (textView.text! == ""){

            textView.text = "No Notes Added"
        }
        
    }
    


    func textFieldDidEndEditing(_ textField: UITextField) {

        if (textField.text! == ""){

            textField.text = "No Notes Added."
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {


        if (scrollview.tag == 1)
        {
            print ("Scrolling ended at: \(scrollView.contentOffset.x/scrollView.frame.width)")
            self.currentItemNumber = Int(scrollView.contentOffset.x/scrollView.frame.width)

            if (self.currentItemNumber != self.prev)
            {
                self.prev = self.currentItemNumber

            }

            setViewsForContact(n:self.currentItemNumber)

        }


    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if (scrollview.tag == 1)
        {
            UIView.animate(withDuration: 0.5, animations: {
                self.contactName.alpha = 0
                self.note.alpha = 0
            })
        }


    }

    override func viewDidAppear(_ animated: Bool) {


        clearView()

        var tt:String = AppDelegate().getTopMostView()

        print ("***\(tt)")

        self.currentItemNumber = 0

        if(tt.contains("Group View"))
        {
            var iid = tt.components(separatedBy: ",")
            self.getContactsForGroup(iid[1])
            self.groupCode = iid[1]
        }

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.userid = AppDelegate().getUserId()
        self.scrollview.delegate = self

        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        self.note.delegate = self



//        NotificationCenter.default.addObserver(self, selector: #selector(MyGps.refreshViews), name: NSNotification.Name(rawValue: "GroupView"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(viewDidAppear(_:)), name: NSNotification.Name(rawValue: "GroupView"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(changePhoto), name: NSNotification.Name(rawValue: "ChangePhoto"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(savePrefs), name: NSNotification.Name(rawValue: "newContact"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(savePrefs), name: NSNotification.Name(rawValue: "addBook"), object: nil)

        // Do any additional setup after loading the view.
    }

    func savePrefs()
    {

        var tt:String = AppDelegate().getTopMostView()

        if(tt.contains("Group View"))
        {
            var iid = tt.components(separatedBy: ",")


            let userDefaults = UserDefaults.standard
            userDefaults.set(self.userid, forKey: "userid")
            userDefaults.set(iid[1], forKey: "groupid")
            userDefaults.synchronize()
        }


        
    }




    func changePhoto(){

        if (self.allContacts.count>0)
        {


            var current_contact = self.allContacts[self.currentItemNumber].contact_id

            print("Image Tapped")
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
                print("Button capture")


                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
                imagePicker.allowsEditing = true

                self.present(imagePicker, animated: true, completion: nil)
            }
            

        }




    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {

            self.im = image

        } else{
            print("Something went wrong")
        }

        //self.dismiss(animated: true, completion: nil)
        
        myImageUploadRequest()
        
        
    }
    

    func myImageUploadRequest()
    {

        AppDelegate().showLoading(str: "Uploading Image...")

        let myUrl:URL!
        myUrl = URL(string: "http://jasontask.azurewebsites.net/api/changepicture.php")!;

        var request = URLRequest(url:myUrl!);
        request.httpMethod = "POST";

        var page = self.currentItemNumber//scrollView.contentOffset.x / scrollView.frame.size.width;

        print("page number= \(page)")

        var currentNumber = Int(page)


        let param = [
            "userid"  : "\(self.userid!)",
            "contactid"    : "\(self.allContacts[currentNumber].contact_id!)",
        ]



        print("myUrl: \(myUrl) params: userid=\(self.userid!), contactid=\(self.allContacts[currentNumber].contact_id!)");


        let boundary = generateBoundaryString()

        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")


        let imageData = UIImageJPEGRepresentation(im, 0)

        if(imageData==nil)  { return; }

        request.httpBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData!, boundary: boundary)



        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            data, response, error in

            AppDelegate().dissLoading()

            if error != nil {

                print("error=\(error)")

                AppDelegate().showMsg(str: "Error", success: false)
                return
            }
            else{
                AppDelegate().showMsg(str: "Contact created", success: true)

            }


            // You can print out response object
            print("******* response = \(response)")

            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")


            DispatchQueue.main.async {

            self.imagePicker.dismiss(animated: false, completion: nil)


            }

            
        }) 
        
        task.resume()
        
    }

    func randomStringWithLength(length: Int) -> String {

        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }

        return randomString
    }


    func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        let body = NSMutableData();

        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }


        let filename = "user-image-\(randomStringWithLength(length: 15)).jpg"

        let mimetype = "image/jpg"

        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")



        body.appendString("--\(boundary)--\r\n")

        return body as Data
    }



    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }




    func keyboardWillHide(_ sender: Notification) {


        if let keyboardSize = (sender.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            //self.view.frame.origin.y = 0
            //self.view.frame.origin.y += keyboardSize.height

            self.view.frame.origin.y += (keyboardSize.height)
        }

    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        print ("Editing Should START")

        if (textField.text! == "No Notes Added."){
            textField.text = ""
        }

        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {

        print ("Editing DID START")

        if (textField.text! == "No Notes Added."){
            textField.text = ""
        }
    }

    func clearView()
    {
        self.view.endEditing(true)
        for subview in scrollview.subviews {
            subview.removeFromSuperview()
        }
        
        self.scrollview.setContentOffset(CGPoint(x: 0, y: 0), animated: true)

        self.note.text = ""
        self.contactName.text = ""
    }



    func keyboardWillShow(_ sender: Notification) {

        if let keyboardSize = (sender.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.view.frame.origin.y -= keyboardSize.height
        }
        
    }

    override func viewWillDisappear(_ animated: Bool) {

        self.view.endEditing(true)

    }

    func getContactsForGroup(_ id: String)
    {
        if (self.scrollview != nil)
        {
            self.scrollview.subviews.map({ $0.removeFromSuperview() })
        }


        UIView.animate(withDuration: 0.5, animations: {
            self.contactName.alpha = 0
            self.note.alpha = 0
            //self.imageBtn.alpha = 1.0
        })

       //GTToast.create("Loading contacts. Please wait...").show()

        AppDelegate().showLoading(str: "Loading Contacts...")

        var apiCall = "http://jasontask.azurewebsites.net/api/index.php?command=loadGroupPeople&user_id=\(self.userid!)&group_id=\(id)"

        print(apiCall)

        Alamofire.request(apiCall)
            .responseJSON { response in

                UIView.animate(withDuration: 0.5, animations: {
                    self.contactName.alpha = 1
                    self.note.alpha = 1
                    //self.imageBtn.alpha = 1.0
                })

                AppDelegate().dissLoading()
                let j  = response.result.value as? NSDictionary

                if (j != nil)
                {
                    var json = j!

                    let result = json["result"] as! [[String : AnyObject]]

                    self.allContacts = []

                    if (!result.isEmpty)
                    {
                        for r in result {

                            let id = r["contact_id"]! as! String
                            let by = r["added_by"]! as! String
                            let gp = r["group_id"]! as! String
                            let name = r["name"]! as! String

                            var picture:String!

                            if (r["picture"] as? String != nil)
                            {
                                picture = r["picture"] as! String
                            }
                            else
                            {
                                picture = "https://d2wnxi2v4fzw0h.cloudfront.net/assets/fallback/preview_default_profile.png"
                            }

                            let notes = r["notes"] as! String //[[String:String]]

                            var allNotes:[[String:String]]
                            allNotes = []



                            var c:contact!
                            c = contact(id:id, by:by, gp:gp, name:name, picture:picture, added:" ", notes:notes)
                            // }

                            self.allContacts.append(c)

                        }

                        print(self.allContacts.count)
                        self.count = self.allContacts.count


                        if (self.allContacts.count>0)
                        {
                            self.noMembers.isHidden = true
                            self.setAllViews()
                            self.setViewsForContact(n: self.currentItemNumber)
                            self.contactName.isHidden = false
                            self.note.isHidden = false
                            self.note.isUserInteractionEnabled = true

                        }
                        else
                        {
                            self.noMembers.isHidden = false
                            self.contactName.isHidden = true
                            self.note.isHidden = true
                            self.note.isUserInteractionEnabled = false

                            AppDelegate().setTopMostView(top: "Group ViewNP,\(self.groupCode!)")

                        }






                    }
                    else
                    {
                        self.noMembers.isHidden = false
                        self.contactName.isHidden = true
                        self.note.isHidden = true
                        self.note.isUserInteractionEnabled = false
                        AppDelegate().setTopMostView(top: "Group ViewNP,\(self.groupCode!)")
                       //GTToast.create("No members in this group.").show()
                    }
                    
                    
                }
                else{
                    self.noMembers.isHidden = false
                    self.contactName.isHidden = true
                    self.note.isHidden = true
                    self.note.isUserInteractionEnabled = false
                    AppDelegate().setTopMostView(top: "Group ViewNP,\(self.groupCode!)")
                   //GTToast.create("No Members for this group").show()
                }
                
                
        }
        
        
    }

    @IBOutlet weak var noMembers: UILabel!
    
   /* func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        if(text == "\n") {
            //textView.resignFirstResponder();


            //editTapped(textView.text!)
            
            
            
            return false;
        }
        
        return true;
    } */

    func setAllViews()
    {
        let navBarHeight = self.navigationController!.navigationBar.frame.size.height
        let scrollviewHeight = self.scrollview.frame.height
        let scrollviewWidth = self.scrollview.frame.width

        self.pgControl.numberOfPages = self.allContacts.count

//        self.pgControl.currentPage = 0
//
//        self.pgControl.updateCurrentPageDisplay()


        self.allImageViews.removeAll()

        for i in 0...self.count-1{

            var view = UIImageView()

            view.frame = CGRect(x: scrollviewWidth*CGFloat(i),y: 0, width: scrollviewWidth, height: scrollviewHeight)

            view.backgroundColor = UIColor(netHex: 0x4a4b50)

            view.contentMode = .scaleAspectFill

            view.clipsToBounds = true

            view.tag = i
            
            self.allImageViews.append(view)
            
            
            //Image Auto Load Disabled
//            Alamofire.request(allContacts[i].picture).responseImage { response in
//             //   debugPrint(response)
//
//
//
//                print("request : \(response.request)");
//                print("allContacts[i].picture: \(self.allContacts[i].picture)");
//                print("TAG: \(view.tag)");
//
//             //   print(response.response)
//             //   debugPrint(response.result)
//
//                if let image = response.result.value {
//                    var v = self.scrollview.viewWithTag(i) as? UIImageView
//                    v?.image = image
//                    v?.setNeedsDisplay()
//                    self.scrollview.setNeedsDisplay()
//                }
//            }
            //TILL HERE


            //let singleTap = UIGestureRecognizer(target: self, action:#selector(TryViewController.createGp(_:)))

            //scrollview.addGestureRecognizer(singleTap)

            //view.note.isUserInteractionEnabled = true

            //view.note.addGestureRecognizer(singleTap)


            //let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(TryViewController.createGp(_:)))
            //view.isUserInteractionEnabled = true
            //view.addGestureRecognizer(tapGestureRecognizer)


            print(i)
            self.scrollview.addSubview(view)
            //self.scrollview.addGestureRecognizer(singleTap)

            //self.scrollview.isUserInteractionEnabled = true


        }

        //let flt:CGFloat = 5.0 + self.scrollview.frame.width

        self.scrollview.contentSize = CGSize(width: self.scrollview.frame.width*CGFloat(self.count), height: self.scrollview.frame.height)
        self.pgControl.currentPage = 0
        self.pgControl.updateCurrentPageDisplay()

        scrollview.isUserInteractionEnabled = true
        
        scrollview.delaysContentTouches = false
        
        scrollview.canCancelContentTouches = false
        

    }

    func editTapped(_ str:String)
    {

        let groupName = str

        if (1 == 1)
        {

            var page = scrollview.contentOffset.x / scrollview.frame.size.width;
            page+=1
            print("page number= \(page)")

            //currentNumber = self.currentItemNumber


            //self.createGroup(self.id, name: groupName!, secretCode: secretCode as String)

            //http://jasontask.azurewebsites.net/api/index.php?command=addNotes&user_id=917093491659259&contact_id=62&note=hellowwor

            self.allContacts[self.currentItemNumber].notes = str

            print("user-id:\(self.userid)")
            print("contact-id:\(self.allContacts[self.currentItemNumber].contact_id)")
            print("note:\(groupName)")


            //AppDelegate().showLoading(str: "Up...")

           GTToast.create("Updating Notes...").show()
            var apiCall = "http://jasontask.azurewebsites.net/api/index.php?command=addNotes&user_id=\(self.userid!)&contact_id=\(self.allContacts[self.currentItemNumber].contact_id!)&note=\(groupName)"

            apiCall = apiCall.replacingOccurrences(of: " ", with: "%20")
            apiCall = apiCall.replacingOccurrences(of: "\n", with: "%0A")

            print(apiCall)

            Alamofire.request(apiCall)
                .responseString { response in

                    AppDelegate().dissLoading()

                    if (response.result.value != nil)
                    {

                        print("Response String: \(response.result.value)")
                       //GTToast.create(response.result.value!).show()
                        //self.allContacts = []
                        
                        if (response.result.value!.range(of: "successfully") != nil)
                        {
                            //self.allContacts[self.currentNumber-1].notes.note = groupName!
                            self.allContacts = []
                            //                self.getContactsForGroup(self.groupID)
                            
                        }
                        
                        //self.getContactsForGroup(self.groupID)
                    }
            }
            
            
        }
        else
        {
            print("Enter note or press cancel")
        }
        
        
        
        
    }

    class contact
    {

        var contact_id:String!
        var added_by:String!
        var group_id:String!
        var name:String!
        var picture:String!
        var date_added:String!
        var notes:String!




        init(id:String, by:String, gp:String, name:String, picture:String, added:String, notes:String)
        {
            self.contact_id = id
            self.added_by = by
            self.group_id = gp
            self.name = name
            self.picture = picture
            self.date_added = added
            self.notes = notes
        }


    }


}
