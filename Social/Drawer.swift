//
//  Drawer.swift
//  Social
//
//  Created by Aqib on 30/01/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import UIKit
import KYDrawerController

class Drawer: UIViewController, UITableViewDelegate, UITableViewDataSource {


    //var options:[String] = ["My Profile", "Invitation Code", "My Groups", "Join Groups", "Notifications", "Sign Out"]

    var options:[String] = ["My Profile", "My Groups", "Join Groups", "Sign Out"]
    
    @IBOutlet weak var optionsTable: UITableView!
    @IBOutlet weak var profilePicture: PASImageView!
    var profileImageUrl:String!
    var userName:String!
    @IBOutlet weak var fullname: UILabel!
    var drawer:KYDrawerController!


    func setDrawer(){

       // drawer = self.navigationController!.parent as! KYDrawerController!

        if let drawerController = parent as? KYDrawerController {
            //drawerController.setDrawerState(.closed, animated: true)
            self.drawer = drawerController
        }

        print ("*Drawer has loaded")


        if let url = UserDefaults.standard.value(forKey: "url")
        {
            profilePicture.imageURL(URL: URL(string: url as! String)!)
        }
        else
        {

        }

        if let name = UserDefaults.standard.value(forKey: "username") {

            self.fullname.text = name as! String
            //self.userName = name as! String

        }
        else{

            self.fullname.text = ""
            
        }



    }

    override func viewDidLoad() {
        super.viewDidLoad()



        NotificationCenter.default.addObserver(self, selector: #selector(self.setDrawer), name: NSNotification.Name(rawValue: "profilePicUpdated"), object: nil)

        setDrawer()

        self.optionsTable.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return self.options.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var item = self.optionsTable.dequeueReusableCell(withIdentifier: "drawerRow") as! DrawerRow
        item.label.text = self.options[indexPath.row]

        return item
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 50
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

//        var storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//
//        var ourVc = storyboard.instantiateViewController(withIdentifier: "MainController") as! MainVC
//

        var selectedOption = options[indexPath.row]

        let u = UserDefaults.standard
        u.set(selectedOption, forKey: "selectedDrawer")
        u.synchronize()

        


//        if (options[indexPath.row] == "Home")
//        {
//            ourVc.segueString = "Home"
//        }

        drawer.setDrawerState(.closed, animated: true)

    }

}
