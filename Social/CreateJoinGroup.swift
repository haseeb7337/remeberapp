//
//  CreateJoinGroup.swift
//  Social
//
//  Created by Aqib on 11/03/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import UIKit
import GTToast
import Alamofire

class CreateJoinGroup: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var titleT: UILabel!
    var initialY:CGFloat = 150
    var appDel: AppDelegate!
    var group_id: String!
    var textReceived:String!
    var action:String = ""
    @IBOutlet weak var tt: UILabel!
    var name:String!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var fieldtext: UITextField!

    override func viewDidAppear(_ animated: Bool) {



        if (self.titleT.text!.contains("CONFIRMATION"))
        {
            //self.fieldtext.text = "Yes"
        }
    }

    override func viewDidDisappear(_ animated: Bool) {

        let def = UserDefaults.standard
        def.removeObject(forKey: "group-id-delete")
        def.synchronize()

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        //self.group_id = UserDefaults.standard.value(forKey: "group-id-delete") as! String

        if let x = UserDefaults.standard.value(forKey: "group-id-delete") as? String
        {

            self.name = UserDefaults.standard.value(forKey: "group-name-delete") as? String

            self.group_id = UserDefaults.standard.value(forKey: "group-id-delete") as! String
            if (self.group_id.contains("L"))
            {
                self.action = "leavegroup"
                self.titleT.text = "LEAVE GROUP"
                self.tt.text = "Are you sure you want leave this group? If so, type LEAVE in the text box below."
                self.deleteBtn.setTitle("LEAVE", for: .normal)
                

                self.group_id.replacingOccurrences(of: "L", with: "")
            }
            else
            {
                self.tt.text = "Are you sure you want to delete this group? If so, type DELETE in the text box below."
                self.action = "delgroup"

                self.deleteBtn.setTitle("DELETE", for: .normal)
                
            }

        }


        self.appDel = AppDelegate()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        self.fieldtext.delegate = self
        // Do any additional setup after loading the view.
    }



    func textFieldDidEndEditing(_ textField: UITextField) {

//        print ("Done")
//        dismissKeyboard()
//
//        if (appDel.getTopMostView() == "CreateJoinGroup")
//        {
//
//
//            if ((textField.text!.characters.count)>0)
//            {
//                self.textReceived = textField.text!
//                processDoneButton()
//            }
//            else
//            {
//                GTToast.create("Enter Text in the Text Field first!").show()
//            }
//
//        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {


        print ("Done")
        dismissKeyboard()

        if (appDel.getTopMostView() == "CreateJoinGroup")
        {


            if ((textField.text!.characters.count)>0)
            {
                self.textReceived = textField.text!
                processDoneButton()
            }
            else
            {
                GTToast.create("Enter Text in the Text Field first!").show()
            }
            
        }

        return true
    }

    @IBAction func processBtnPressed(_ sender: UIButton) {

        if ((fieldtext.text!.characters.count)>0)
        {
            self.textReceived = fieldtext.text!
            processDoneButton()
        }
        else
        {
            GTToast.create("Enter Text in the Text Field first!").show()
        }


    }


    func processDoneButton()
    {


        if (self.titleT.text == "CREATE GROUP")
        {
            //self.openGroup(id: "541")
            self.createGp(AppDelegate().getUserId(), name: self.textReceived , secretCode: randomStringWithLength(6))
        }
        else if (self.titleT.text == "JOIN GROUP")
        {
            self.joinGp(AppDelegate().getUserId(), secretCode: self.fieldtext.text!)
        }
        else if (self.titleT.text!.contains("INVITATION"))
        {
            self.invitCode(AppDelegate().getUserId(), name: self.textReceived , secretCode: randomStringWithLength(6))
        }
        else
        {
            if (self.fieldtext.text!.lowercased() == "delete")
            {self.deleteGroup()}

            if (self.fieldtext.text!.lowercased() == "leave")
            {self.leaveGroup()}



        }

    }

    func deleteGroup()
    {
        appDel.showLoading(str: "Deleting Group")
        let id = "https://jasontask.azurewebsites.net/api/index.php?command=delgroup&groupid=\(self.group_id!)"


        Alamofire.request(id).responseJSON(completionHandler: { (response) in

            var json =  (response.result.value as! NSDictionary)

            if (json["message"] as! String == "success")
            {
                //self.allGroups.remove(at: num)
                self.navigationController?.popViewController(animated: true)
                GTToast.create("Group Deleted!").show()
            }
            else{
                GTToast.create("Error deleting group").show()
            }

            //self.tableView.reloadData()
        })




        

    }


    func leaveGroup()
    {


        var xx = self.group_id.replacingOccurrences(of: "L", with: "")

        let id = "https://jasontask.azurewebsites.net/api/index.php?command=leavegroup&groupid=\(xx)"


        Alamofire.request(id).responseJSON(completionHandler: { (response) in

            if ((response.result.value as? NSDictionary) != nil)
            {

                var json =  (response.result.value as! NSDictionary)

                if (json["message"] as! String == "success")
                {
                    //self.allGroups.remove(at: num)
                    self.navigationController?.popViewController(animated: true)
                    GTToast.create("Group Left!").show()
                }
                else{
                    GTToast.create("Error leaving group").show()
                }


            }
            else
            {
                GTToast.create("Network Error").show()
            }


            //self.tableView.reloadData()
        })
        
        
        
        
        
        
    }


    func createGp(_ id: String, name: String, secretCode: String)
    {

        appDel.showLoading(str: "Creating Group")
        //GTToast.create("Creating Group. Please wait...").show()
        var apiCall = "https://jasontask.azurewebsites.net/api/index.php?command=createGroup&own="
        apiCall += id
        apiCall += "&name="
        apiCall += name
        apiCall += "&code="
        apiCall += secretCode
        apiCall += "&members=0"


        apiCall = apiCall.replacingOccurrences(of: " ", with: "%20")

        print(apiCall)


        Alamofire.request(apiCall)
            .responseString { response in

                if (response.result.value != nil)
                {
                    print("Response String: \(response.result.value)")
                    //self.appDel.showMsg(str: response.result.value!, success: true)
                    //GTToast.create(response.result.value!).show()

                    var json = response.result.value as! NSDictionary
                    var msg = json.value(forKey: "msg") as! String
                    var gp_id = json.value(forKey: "group_id") as? String

                    self.appDel.showMsg(str: msg, success: true)


                    if (gp_id != nil)
                    {
                        self.openGroup(id: gp_id!)
                    }

                }
                else{

                }
        }





    }


    func openGroup(id: String)
    {
        //print (self.parent)
        (self.parent as! MainVC).segueToGroup(id: id)
    }

    func joinGp(_ id: String, secretCode: String)
    {

        appDel.showLoading(str: "Joining Group")
        //GTToast.create("Creating Group. Please wait...").show()
        var apiCall = "https://jasontask.azurewebsites.net/api/index.php?command=joinGroup&user_id="
        apiCall += id
        apiCall += "&group_code="
        apiCall += secretCode


        apiCall = apiCall.replacingOccurrences(of: " ", with: "%20")

        print(apiCall)


        Alamofire.request(apiCall)
            .responseString { response in

                if (response.result.value != nil)
                {
                    print("Response String: \(response.result.value)")
                    //self.appDel.showMsg(str: response.result.value!, success: true)
                    //GTToast.create(response.result.value!).show()

                    /*if (response.result.value!.contains("successfully"))
                    {
                        openGroup()
                    }*/

                    var json = response.result.value as! NSDictionary
                    var msg = json.value(forKey: "msg") as! String
                    var gp_id = json.value(forKey: "group_id") as? String

                    self.appDel.showMsg(str: msg, success: true)


                    if (gp_id != nil)
                    {
                        self.openGroup(id: gp_id!)
                    }

                }
                else{
                    
                }
        }
        
        



    }

    func invitCode(_ id: String, name: String, secretCode: String)
    {

    }

    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    func keyboardWillShow(_ sender: Notification) {


        let info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        self.view.frame.origin.y = initialY - CGFloat(keyboardFrame.height)

    }

    func keyboardWillHide(_ sender: Notification) {


        let info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        self.view.frame.origin.y = 0//(self.navigationController?.navigationBar.frame.size.height)!


    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func cancelButtonPressed(_ sender: UIButton) {

        if (self.titleT.text == "CREATE GROUP")
        {
            (self.parent as! MainVC).backButton(v: (self.parent as! MainVC).createGp)

            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showMenu"), object: nil)
            
        }else if (self.titleT.text == "JOIN GROUP"){
            (self.parent as! MainVC).backButton(v: (self.parent as! MainVC).joinGroup)
        } else if (self.titleT.text!.contains("INVITATION")) {

            (self.parent as! MainVC).backButton(v: (self.parent as! MainVC).invitationcodes)

        } else {
            self.navigationController?.popViewController(animated: true)
        }




    }

    func randomStringWithLength (_ len : Int) -> String {

        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

        let randomString : NSMutableString = NSMutableString(capacity: len)

        for i in 0 ..< len
        {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        
        return randomString as! String
        
        //return "1234"
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
