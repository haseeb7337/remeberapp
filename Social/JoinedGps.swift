//
//  JoinedGps.swift
//  Social
//
//  Created by Aqib on 11/03/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import GTToast

class JoinedGps: UIViewController, IndicatorInfoProvider , UITableViewDelegate, UITableViewDataSource {


    var appDel: AppDelegate!
    var tableView: UITableView!
    var allGroups: [group] = []
    var id:String!
    var isLoading = false


    var itemInfo = IndicatorInfo(title: "Joined Groups")

    public func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {

        return itemInfo

        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return allGroups.count
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        print ("Parent: \(self.parent?.parent)")
        var gp_id = self.allGroups[indexPath.row].group_id


        (self.parent?.parent as! MainVC).segueToGroup(id: gp_id!)
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell = self.tableView.dequeueReusableCell(withIdentifier: "noteCell") as! NotesTableViewCell

        cell.noteText.text = "\(self.allGroups[indexPath.row].group_name!) (\(self.allGroups[indexPath.row].members_count!))"

        cell.shareBtn.isHidden = true

        //cell.leaveBtn.isHidden = true

        cell.leaveBtn.addTarget(self, action: #selector(JoinedGps.deleteButtonAction(_:)), for: UIControlEvents.touchUpInside)

        cell.leaveBtn.imageView?.contentMode = .scaleAspectFit

        cell.leaveBtn.tag = indexPath.row

        return cell
    }

    func deleteButtonAction(_ sender:UIButton!)
    {


        let num:Int = sender.tag
        let id = self.allGroups[num].group_id!
        let name = self.allGroups[num].group_name!


        let def = UserDefaults.standard
        def.set(id+"L", forKey: "group-id-delete")
        def.set(name, forKey: "group-name-delete")
        def.synchronize()

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GroupDelete"), object: nil)


        //        self.superclass.performSegue(withIdentifier: "DeleteGroup", sender: self)

        //        let num:Int = sender.tag
        //        let id = "https://jasontask.azurewebsites.net/api/index.php?command=delgroup&groupid=\(self.allGroups[num].group_id!)"
        //
        //
        //
        //        let alert = UIAlertController(title: "Confirmation", message: "Are you sure you want to delete this group?", preferredStyle: UIAlertControllerStyle.alert)
        //
        //
        //        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
        //
        //
        //
        //            Alamofire.request(id).responseJSON(completionHandler: { (response) in
        //
        //                var json =  (response.result.value as! NSDictionary)
        //
        //                if (json["message"] as! String == "success")
        //                {
        //                    self.allGroups.remove(at: num)
        //                    GTToast.create("Group Deleted!").show()
        //                }
        //                else{
        //                    GTToast.create("Error deleting group").show()
        //                }
        //
        //                self.tableView.reloadData()
        //            })
        //
        //
        //        }))
        //
        //        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
        //
        //            alert.dismiss(animated: true, completion: nil)
        //        }))
        //
        //        self.present(alert, animated: true, completion: nil)
        //
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        appDel = AppDelegate()

        self.view.backgroundColor = UIColor.black
        let v = GroupsNib.instanceFromNib()
        self.view.addSubview(v)
        self.tableView = v.list

        self.tableView.register(UINib(nibName: "NotesTableViewCell", bundle: nil), forCellReuseIdentifier: "noteCell")


        self.tableView.dataSource = self
        self.tableView.delegate = self

        NotificationCenter.default.addObserver(self, selector: #selector(JoinedGps.viewDidAppear(_:)), name: NSNotification.Name(rawValue: "MyGroups"), object: nil)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {

        if let id = UserDefaults.standard.value(forKey: "id")
        {
            print ("* \(AppDelegate().getTopMostView())")
            if (!self.isLoading && AppDelegate().getTopMostView() == "My Groups")
            {
                self.getAllGroups(id as! String)
                self.isLoading = true

            }

        }

    }

    func getAllGroups(_ id: String)
    {


        self.appDel.showLoading(str: "Loading Groups")
        var apiCall = "http://jasontask.azurewebsites.net/api/index.php?command=joinedGroup&user_id="
        apiCall += id

        print(apiCall)

        Alamofire.request(apiCall)
            .responseJSON { response in

                self.allGroups = []

                self.isLoading = false

                self.appDel.dissLoading()

                print (response.result.value)

                if let json = response.result.value as? NSDictionary{

                    let result = json["result"]! as! [[String : AnyObject]]

                    if (!result.isEmpty)
                    {
                        for r in result {
                            let group_id = r["group_id"]! as! String
                            //let created_by = r["created_by"]! as! String
                            var created_by = ""
                            let group_name = r["group_name"]! as! String
                            let group_code = r["group_code"]! as! String
                            let members_count = r["members_count"]! as! String
                            let date_created = " "//r["date_created"]! as! String
                            let join_id = " "//r["join_id"]! as! String


                            let gp = group(id: group_id, by: created_by, name: group_name, code:group_code, count:members_count, created:date_created, joined:join_id )

                            self.allGroups.append(gp)

                        }

                        print(self.allGroups.count)
                        self.tableView.reloadData()
                        //self.performSegueWithIdentifier("joinedGroups", sender: self)
                        
                    }
                    else
                    {
                        GTToast.create("You haven't joined any group").show()
                    }

                }

        }


    }

    class group
    {

        var group_id:String!
        var created_by:String!
        var group_name:String!
        var group_code:String!
        var members_count:String!
        var date_created:String!
        var join_id:String!


        init(id: String, by: String, name: String, code:String, count:String, created:String, joined:String)
        {
            self.group_id = id
            self.created_by = by;
            self.group_name = name
            self.group_code = code
            self.members_count = count
            self.date_created = created
            self.join_id = joined
            
        }
        
        
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
