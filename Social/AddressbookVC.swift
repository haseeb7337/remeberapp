//
//  AddressbookVC.swift
//  Social
//
//  Created by Aqib on 30/12/2016.
//  Copyright © 2016 root-dev. All rights reserved.
//

import UIKit
import EPContactsPicker

class AddressbookVC: UIViewController,  EPPickerDelegate
{

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var contact:EPContact!

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let destinationNavigationController = segue.destination as! UINavigationController

        let vc = destinationNavigationController.topViewController as! NewContact
        vc.contactName = contact.displayName()
        vc.iii = self.contact.profileImage


    }

    func epContactPicker(_: EPContactsPicker, didSelectContact contact: EPContact) {

        self.contact = contact
        //self.performSegue(withIdentifier: "selectedContact", sender: self)


    }


    func epContactPicker(_: EPContactsPicker, didCancel error: NSError) {

        self.navigationController?.popViewController(animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {

//        if let xx = UserDefaults.standard.value(forKey: "contactAdded")
//        {
//            var yy = xx as! String
//            if (yy == "yes")
//            {
//
//                let userDefaults = UserDefaults.standard
//                userDefaults.set("no", forKey: "contactAdded")
//                userDefaults.synchronize()
//                self.navigationController?.popViewController(animated: true)
//            }
//        }


    }

    override func viewDidLoad() {
        super.viewDidLoad()


        self.indicator.startAnimating()

        let contactPickerScene = EPContactsPicker(delegate: self, multiSelection:false, subtitleCellType: SubtitleCellValue.email)
        let navigationController = UINavigationController(rootViewController: contactPickerScene)
        self.present(navigationController, animated: true, completion: nil)


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
