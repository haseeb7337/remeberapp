//
//  NewContact.swift
//  FaceFlip
//
//  Created by Aqib on 15/11/2015.
//  Copyright © 2015 CodeArray. All rights reserved.
//

import UIKit
import EPContactsPicker




class NewContact: UIViewController, UINavigationControllerDelegate,UIImagePickerControllerDelegate,  EPPickerDelegate {
    
    @IBOutlet weak var v: UIView!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    var contactName:String!
    var imagePicker = UIImagePickerController()
    var userid:String!
    var groupid:String!

    var iii:UIImage?

    var contact:EPContact!

    var open:Bool = false
    
    
    @IBOutlet weak var contactImage: UIImageView!
    
    @IBAction func closeView(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func addContact(_ sender: UIButton) {
        
        if (nametext.text != "")
        {
            v.isHidden = false
            myImageUploadRequest()
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Please enter the name first.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }

    func epContactPicker(_: EPContactsPicker, didSelectContact contact: EPContact) {

        self.contact = contact

        if (contact.profileImage != nil)
        {
                self.contactImage.image = contact.profileImage!
        }

        self.nametext.text = contact.displayName()
    }
    
    
    @IBOutlet weak var nametext: UITextField!

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {

            contactImage.image = image


        } else{
            print("Something went wrong")
        }

        self.dismiss(animated: true, completion: nil)
        
//        myImageUploadRequest()

        
    }


    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
        contactImage.image = image
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (self.open)
        {
            let contactPickerScene = EPContactsPicker(delegate: self, multiSelection:false, subtitleCellType: SubtitleCellValue.email)
            let navigationController = UINavigationController(rootViewController: contactPickerScene)
            self.present(navigationController, animated: true, completion: nil)
        }
        
        //myActivityIndicator.hidden = true
        
        if let id: AnyObject = UserDefaults.standard.object(forKey: "id") as AnyObject? {
            self.userid = id as! String
            
        }
        else
        {
            print("not set")
        }
        
        if let id: AnyObject = UserDefaults.standard.object(forKey: "currentGpID") as AnyObject? {
            self.groupid = id as! String
            
        }
        
        if (contactName != nil)
        {
            nametext.text = contactName
        }
        // Do any additional setup after loading the view.
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(NewContact.imageTapped(_:)))
        contactImage.isUserInteractionEnabled = true
        contactImage.addGestureRecognizer(tapGestureRecognizer)
        


        //self.contactImage.image = self.iii!
        
    }
    
    
    func imageTapped(_ img: AnyObject)
    {
        print("Image Tapped")
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Button capture")
            
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func myImageUploadRequest()
    {

        AppDelegate().showLoading(str: "Uploading Image...")
        var name:String = nametext.text!
        let myUrl = URL(string: "http://jasontask.azurewebsites.net/api/upload.php");
        //let myUrl = NSURL(string: "https://lfk.azurewebsites.net/upload.php");
        
        var request = URLRequest(url:myUrl!);
        request.httpMethod = "POST";
        
        print ("userid:  \(userid!), groupid: \(groupid!), name:\(name)")
        
        let param = [
            "userid"  : "\(userid!)",
            "group"    : "\(groupid!)",
            "name"    : "\(name)"
        ]
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        let imageData = UIImageJPEGRepresentation(contactImage.image!, 1)
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData!, boundary: boundary)
        
        
        //myActivityIndicator.isHidden = false
        
        //myActivityIndicator.startAnimating();
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            data, response, error in

            AppDelegate().dissLoading()

            if error != nil {

                print("error=\(error)")

                AppDelegate().showMsg(str: "Error", success: false)
                return
            }
            else{
                AppDelegate().showMsg(str: "Contact created", success: true)

            }


            // You can print out response object
            print("******* response = \(response)")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
            
            //self.performSegueWithIdentifier("contactAdded", sender: self)
            
            let userDefaults = UserDefaults.standard
            userDefaults.set(true, forKey: "contactAdded")
            userDefaults.synchronize()
            
            
            
            self.dismiss(animated: true, completion: nil);
            
            //self.performSegueWithIdentifier("contactHasBeenAddedYes", sender: self)
            
            //unwindForSegue(<#T##unwindSegue: UIStoryboardSegue##UIStoryboardSegue#>, towardsViewController: <#T##UIViewController#>)
            
            //            do{
            //             let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
            //                print(json)
            //            } catch {
            //                
            //            }
            
            
            
            
            
            //            dispatch_async(dispatch_get_main_queue(),{
            //                self.myActivityIndicator.stopAnimating()
            //                    self.contactImage.image = nil;
            //            });
            
            /*
            if let parseJSON = json {
            var firstNameValue = parseJSON["firstName"] as? String
            println("firstNameValue: \(firstNameValue)")
            }
            */
            
        }) 
        
        task.resume()

    }
    
    func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        var body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        
        let filename = "user-image-\(randomStringWithLength(length: 15)).jpg"
        
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")
        
        
        
        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
    }
    
    func randomStringWithLength(length: Int) -> String {

        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }

        return randomString
    }

    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
