//
//  MyGroups.swift
//  Social
//
//  Created by Aqib on 11/03/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MyGroups: ButtonBarPagerTabStripViewController {

    @IBOutlet weak var buttonsBar: ButtonBarView!

    override func viewDidLoad() {

        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = UIColor(netHex: 0x15A4F6)
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        super.viewDidLoad()


        NotificationCenter.default.addObserver(self, selector: #selector(dlt), name: NSNotification.Name(rawValue: "GroupDelete"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(newContact), name: NSNotification.Name(rawValue: "newContact"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(addBook), name: NSNotification.Name(rawValue: "addBook"), object: nil)
        

        // Do any additional setup after loading the view.
    }

    func newContact()
    {
        self.performSegue(withIdentifier: "newContact", sender: self)
    }

    func addBook()
    {
        self.performSegue(withIdentifier: "addBook", sender: self)
    }

    func dlt()
    {

        
        self.performSegue(withIdentifier: "DeleteGroup", sender: self)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = MyGps()
        let child_2 = JoinedGps()


        return [child_1, child_2]
    }


    


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {


        if (segue.identifier == "addBook")
        {
            let des = segue.destination as! NewContactsVS

            des.addBook = true
        }
        if (segue.identifier == "newContact")
        {
            let des = segue.destination as! NewContactsVS

            des.addBook = false
        }

    }


}
