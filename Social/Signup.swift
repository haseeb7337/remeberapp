//
//  Signup.swift
//  Social
//
//  Created by Aqib on 18/12/2016.
//  Copyright © 2016 root-dev. All rights reserved.
//

import UIKit



class Signup: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var picView: UIImageView!
    @IBOutlet weak var abput: UITextField!
    @IBOutlet weak var repass: UITextField!
    @IBOutlet weak var pass: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var sname: UITextField!

    @IBOutlet weak var pic: UIButton!
    var initialY:CGFloat!
    var apiCall:String = "http://jasontask.azurewebsites.net/api/signup.php"
    let imagePicker = UIImagePickerController()

    var img:UIImage!

    @IBAction func closeWindow(_ sender: UIBarButtonItem) {

       self.dismiss(animated: true, completion: nil)


    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {

            img = image

            self.picView.image = img


        } else{
            print("Something went wrong")
        }


        self.dismiss(animated: true, completion: nil)
        
    }






    func signupApiCall()
    {

    }

    @IBAction func didPressSignup(_ sender: UIButton) {

        if (self.sname.text!.length<1)
        {

        } else if (self.email.text!.length<1)
        {

        } else if (self.pass.text!.length<1)
        {

        } else if (self.repass.text!.length<1)
        {

        } else if (self.abput.text!.length<1)
        {

        } else if (self.img == nil)
        {

        } else{

            myImageUploadRequest()

        }
    }

    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    func keyboardWillShow(_ sender: Notification) {


        let info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        self.view.frame.origin.y = self.initialY - CGFloat(keyboardFrame.height) + 100
        
    }



    func keyboardWillHide(_ sender: Notification) {


        let info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        //        UIView.animateWithDuration(0.1, animations: { () -> Void in
        //            self.bottomConstraint.constant = keyboardFrame.size.height + 20
        //        })


        //totalUplift += 50
        self.view.frame.origin.y = self.initialY
    }


    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if (textField.tag != 105)
        {

            if let theLabel = self.view.viewWithTag(textField.tag+1) as? UITextField {

                theLabel.becomeFirstResponder()
            }

        }
        else{
            self.didPressSignup(UIButton())
            self.dismissKeyboard()
        }



        return true

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        

        self.sname.delegate = self
        self.email.delegate = self
        self.pass.delegate = self
        self.repass.delegate = self
        self.abput.delegate = self

        self.imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
        imagePicker.allowsEditing = true



        self.initialY = self.view.frame.origin.y

        let center: NotificationCenter = NotificationCenter.default
        
        center.addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pictureChooser(_ sender: UIButton) {


        present(imagePicker, animated: true, completion: nil)
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func myImageUploadRequest()
    {

        AppDelegate().showLoading(str: "Signing up...")
        let myUrl = NSURL(string: self.apiCall);

        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";

        let param = [
            "username"  : self.sname.text!,
            "email"    : self.email.text!,
            "password"    : self.pass.text!,
            "about"    : self.abput.text!

        ]

        let boundary = generateBoundaryString()

        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")


        let imageData = UIImageJPEGRepresentation(img, 1)


        if(imageData==nil)  { return; }


        request.httpBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData!, boundary: boundary)

//        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "file", imageDataKey: imageData! as NSData, boundary: boundary) as Data


        //myActivityIndicator.startAnimating();

        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in

            if error != nil {
                print("error=\(error)")
                return
            }

            // You can print out response object
            print("******* response = \(response)")

            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")

            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary

                print(json)

                AppDelegate().dissLoading()
                AppDelegate().showMsg(str: "Account Created", success: true)
                self.navigationController?.dismiss(animated: true, completion: nil)

                DispatchQueue.main.async {
                    //self.myActivityIndicator.stopAnimating()
                    //self.myImageView.image = nil;
                }

            }catch
            {
                print(error)
            }

        }

        task.resume()
    }


    func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        let body = NSMutableData();

        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }


        let filename = "user-image-\(randomString(length: 15)).jpg"

        let mimetype = "image/jpg"

        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")



        body.appendString("--\(boundary)--\r\n")

        return body as Data
    }



    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }

    func randomString(length: Int) -> String {

        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }




}
