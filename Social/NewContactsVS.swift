//
//  NewContactsVS.swift
//  Social
//
//  Created by Aqib on 17/03/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import UIKit
import EPContactsPicker
import GTToast

class NewContactsVS: UIViewController, EPPickerDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate  {


    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var loadingContacts: UIView!
    @IBOutlet weak var imgField: UIImageView!
    @IBOutlet weak var nameField: UITextField!
    var addBook:Bool!
    var name: String?
    var img: UIImage?

    var initialY:CGFloat = 100

    var userid:String!
    var groupid:String!

    var imagePicker = UIImagePickerController()

    var contact:EPContact!



    override func viewWillDisappear(_ animated: Bool) {

        self.view.endEditing(true)
        
    }

    func epContactPicker(_: EPContactsPicker, didCancel error: NSError)
    {
        self.navigationController!.popViewController(animated: true)
    }

    func epContactPicker(_: EPContactsPicker, didSelectContact contact: EPContact) {

        self.loadingContacts.isHidden = true

        self.contact = contact

        if (contact.profileImage != nil)
        {
            self.img = contact.profileImage
            self.imgField.image = contact.profileImage!
        }

        if (contact.displayName() != nil)
        {
            self.name = contact.displayName()
            self.nameField.text = contact.displayName()
        }

    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {



        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {

            self.imgField.image = image


        } else{
            print("Something went wrong")
        }

        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)

        //        myImageUploadRequest()
        
        
    }


    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){

        self.dismiss(animated: true, completion: { () -> Void in

            self.imgField.image = image
        })



    }

    override func viewDidAppear(_ animated: Bool) {

        self.view.endEditing(true)

//        if (name != nil)
//        {
//            self.nameField.text = self.name!
//        }
//
//        if (img != nil)
//        {
//            self.imgField.image = img!
//        }
    }


    @IBAction func cancelPressed(_ sender: UIButton) {

        self.navigationController!.popViewController(animated: true)
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        if (self.addBook!)
        {

            let contactPickerScene = EPContactsPicker(delegate: self, multiSelection:false, subtitleCellType: SubtitleCellValue.email)
            let navigationController = UINavigationController(rootViewController: contactPickerScene)
            self.present(navigationController, animated: true, completion: nil)

            self.spinner.startAnimating()
            
        }
        else
        {
            self.loadingContacts.isHidden = true
        }


        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(NewContact.imageTapped(_:)))
        self.imgField.isUserInteractionEnabled = true
        self.imgField.addGestureRecognizer(tapGestureRecognizer)

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)


        let center: NotificationCenter = NotificationCenter.default

        center.addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)






    }

    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    func keyboardWillShow(_ sender: Notification) {


        let info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        self.view.frame.origin.y = initialY - CGFloat(keyboardFrame.height)

    }

    func keyboardWillHide(_ sender: Notification) {


        let info = sender.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        self.view.frame.origin.y = self.navigationController!.navigationBar.frame.size.height//(CGFloat(keyboardFrame.height))
        
    }

    func imageTapped(_ img: AnyObject)
    {
        print("Image Tapped")
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Button capture")


            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            imagePicker.allowsEditing = true

            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func myImageUploadRequest()
    {

        self.dismissKeyboard()
        AppDelegate().showLoading(str: "Uploading Image...")
        var name:String = nameField.text!
        let myUrl = URL(string: "http://jasontask.azurewebsites.net/api/upload.php");
        //let myUrl = NSURL(string: "https://lfk.azurewebsites.net/upload.php");

        var request = URLRequest(url:myUrl!);
        request.httpMethod = "POST";


        userid = UserDefaults.standard.value(forKey: "userid") as! String
        groupid = UserDefaults.standard.value(forKey: "groupid")as! String


        print ("userid:  \(userid!), groupid: \(groupid!), name:\(name)")

        let param = [
            "userid"  : "\(userid!)",
            "group"    : "\(groupid!)",
            "name"    : "\(name)"
        ]

        let boundary = generateBoundaryString()

        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")


        let imageData = UIImageJPEGRepresentation(imgField.image!, 1)

        if(imageData==nil)  { return; }

        request.httpBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData!, boundary: boundary)


        //myActivityIndicator.isHidden = false

        //myActivityIndicator.startAnimating();

        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            data, response, error in

            AppDelegate().dissLoading()

            if error != nil {

                print("error=\(error)")

                AppDelegate().showMsg(str: "Error", success: false)
                return
            }
            else{
                //AppDelegate().showMsg(str: "Contact created", success: true)

            }


            // You can print out response object
            print("******* response = \(response)")

            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")

            //self.performSegueWithIdentifier("contactAdded", sender: self)

            let userDefaults = UserDefaults.standard
            userDefaults.set(true, forKey: "contactAdded")
            userDefaults.synchronize()


            DispatchQueue.main.async {

                self.navigationController?.popViewController(animated: true)
            }



            //self.navigationController?.popViewController(animated: true)
            //GTToast.create("Contact Added!").show()
            
            //self.dismiss(animated: true, completion: nil);


        })

        task.resume()

    }

    func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        var body = NSMutableData();

        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }


        let filename = "user-image-\(randomStringWithLength(length: 15)).jpg"

        let mimetype = "image/jpg"

        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")



        body.appendString("--\(boundary)--\r\n")

        return body as Data
    }

    func randomStringWithLength(length: Int) -> String {

        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }

        return randomString
    }
    
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }


    @IBAction func addContact(_ sender: UIButton) {

        if (nameField.text != "")
        {
            
            myImageUploadRequest()
            sender.isEnabled = false
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Please enter the name first.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
