//
//  GroupsNib.swift
//  Social
//
//  Created by Aqib on 12/03/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import UIKit


class GroupsNib: UIView {


    class func instanceFromNib() -> GroupsNib {
        return UINib(nibName: "Groups", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! GroupsNib
    }


    @IBOutlet weak var list: UITableView!


    

}
