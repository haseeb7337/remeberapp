//
//  SocialVC.swift
//  Social
//
//  Created by Aqib on 21/04/2017.
//  Copyright © 2017 root-dev. All rights reserved.
//

import UIKit
import FacebookCore
import FBSDKCoreKit
import Alamofire

class SocialVC: UIViewController , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var status: UILabel!
    var friends: [NSDictionary] = []
    @IBOutlet weak var table: UITableView!
    var nextC:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        checkFbLogin()
        self.table.delegate = self
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0)
        {
            return friends.count
        }
        else
        {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell = self.table.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        if (indexPath.section == 1)
        {
            cell.textLabel?.text = "Load More"
        }
        else
        {
            cell.textLabel?.text = self.friends[indexPath.row]["name"] as! String
        }

        return cell

    }

    func loadMore()
    {

        print("Loading More...")
        Alamofire.request(URL(string: self.nextC)!).responseJSON { (responseData) in


            debugPrint(responseData)

            if (responseData.result.value as? NSDictionary != nil)
            {
                var res = responseData.result.value as! NSDictionary

                var data = res.value(forKey: "data") as! [NSDictionary]

                var pagging = res.value(forKey: "paging") as! NSDictionary

                if (pagging.value(forKey: "next") != nil)
                {
                    self.nextC = pagging.value(forKey: "next") as! String
                }
                else
                {
                    self.nextC = ""
                }

                print("Received!")

                print (data.count)

                self.friends += data
                
                self.table.reloadData()
            }
        }
    }

    func checkFbLogin(){

        if let accessToken = AccessToken.current {
            // User is logged in, use 'accessToken' here.

            self.status.text = "Logged In"

        }
        else{
            self.status.text = "NOT Logged In"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if (indexPath.section == 1 && self.nextC != "")
        {
            self.loadMore()
        }
    }

    @IBAction func getAllFriends(_ sender: UIButton) {


        print("Getting Friends")
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath:  "me/taggable_friends", parameters:["fields": "id, name, picture", "type":"name", "q":"Affan"])


        graphRequest.start(completionHandler: { (connection, r, error) -> Void in

            if ((error) != nil)
            {
                // Process error
                print ("ERROR: \(error)")

            }
            else
            {

                var res = r as! NSDictionary

                var data = res.value(forKey: "data") as! [NSDictionary]

                var pagging = res.value(forKey: "paging") as! NSDictionary

                if (pagging.value(forKey: "next") != nil)
                {
                    self.nextC = pagging.value(forKey: "next") as! String
                }
                else
                {
                    self.nextC = ""
                }

                print (data.count)

                self.friends += data

                self.table.reloadData()
                
            }
        })

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
