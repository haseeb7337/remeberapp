//
//  AppDelegate.swift
//  Social
//
//  Created by Aqib on 15/11/2016.
//  Copyright © 2016 root-dev. All rights reserved.
//

import UIKit
import FacebookLogin
import SVProgressHUD
import FBSDKCoreKit
import Instabug
import Fabric
import Crashlytics
import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var topMostView = ""

    func getTopMostView() -> String
    {
        if let tt = UserDefaults.standard.object(forKey: "top") as! String?
        {
            return tt
        }
        else
        {
            return "My Profile"
        }

    }

    func setTopMostView(top: String)
    {
        print("Setting GroupId: \(top)")
        let userDefaults = UserDefaults.standard
        userDefaults.set(top, forKey: "top")
        userDefaults.synchronize()
    }

    func showMsg(str: String, success: Bool)
    {
        if (success){
        SVProgressHUD.showSuccess(withStatus: str)
        }
        else{
            SVProgressHUD.showError(withStatus: str)
        }
    }

    func getUserId() -> String
    {
        if let tt = UserDefaults.standard.object(forKey: "id") as! String?
        {
            return tt
        }
        else
        {
            return "N"
        }

    }

    func showLoading(str:String) {

        SVProgressHUD.show(withStatus: str)
    }

    func dissLoading()
    {
        SVProgressHUD.dismiss()
    }



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {

        var a = LIApplication.init()

        a.clientId = "81rfapzu3mfh5o"
        a.clientSecret = "r6MqrDww5M3zd7ft"
        a.redirectURL  = "https://com.appcoda.linkedin.oauth/oauth"
        a.state =  "1"

        LIExplorer.registerKey("LIExplorer_Public", with: a, staging: true)

        Instabug.start(withToken: "a054440c4e9cfccd83c2a203f79dcafb", invocationEvent: .shake)

      Fabric.with([Crashlytics.self,Answers.self])
      

        return true
    }

//    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
//        if LISDKCallbackHandler.shouldHandle(url as URL!) {
//            return LISDKCallbackHandler.application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
//        }
//        return true
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //AppEventsLogger.activate(application)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {

        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

}

