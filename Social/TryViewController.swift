extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

//
//  TryViewController.swift
//  FaceFlip
//
//  Created by Aqib on 10/11/2015.
//  Copyright © 2015 CodeArray. All rights reserved.
//

import UIKit
import GTToast
import Alamofire

import Foundation
import SCLAlertView




class TryViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate , UITextViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var imgChange: UIButton!
    var id:String!
    var im:UIImage!
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var viewNotesBtn: UIButton!
    @IBOutlet weak var uploadingDialog: UIView!
    var noteOnIndex:String!
    @IBOutlet weak var navBar: UINavigationItem!
    @IBOutlet weak var scrollView: UIScrollView!
    var count:Int = 0;
    var groupID:String = "abc"
    var allContacts: [contact] = []
    var containerView = UIView()
    var userid:String!
    var currentNumber:Int!


    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        var w = scrollView.contentOffset.x+self.view.frame.width
        print ("\(w)")


        if (w>scrollView.contentSize.width)
        {
            print ("*****")
            self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        }


    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "\n") {
            textView.resignFirstResponder();
            
            
            editTapped(textView.text)
            
            
            
            return false;
        }
        
        return true;
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        
        
        return .none
    }
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return 10
        
    }
    
    func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        
        let filename = "user-image-\(randomStringWithLength(length: 15)).jpg"
        
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")
        
        
        
        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
    }
    
    func randomStringWithLength(length: Int) -> String {

        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }

        return randomString
    }

    
    
    @available(iOS 2.0, *)
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        //
        //        if (cell == nil)
        //        {
        tableView.register(UINib.init(nibName: "NotesTableViewCell", bundle: nil), forCellReuseIdentifier: "noteCell")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell") as! NotesTableViewCell
        // }
        
        //cell.
        
        return cell
    }
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "showNotes")
        {
            let destination = segue.destination 
            let controller = destination.popoverPresentationController
            
            if (controller != nil)
            {
                controller?.delegate = self
            }
        }

        if (segue.identifier == "address")
        {


            let destinationNavigationController = segue.destination as! UINavigationController


            let destination = destinationNavigationController.topViewController as! NewContact
            destination.open = true

           // self.performSegue(withIdentifier: "addressB", sender: self)

        }
    }
    @IBAction func changePhoto(_ sender: UIButton) {
        
        print("Image Tapped")
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Button capture")
            
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func viewNotes(_ sender: AnyObject) {
        
        self.performSegue(withIdentifier: "showNotes", sender: self)
    }
    

    func createGp(_ img: AnyObject)
    {
        print("YESS")
        //createGroupButtonDown(UIButton())
    }

    @IBAction func ViewNotesForPerson(_ sender: UIButton) {
        
        
        if (sender.tag == 0)
        {
            sender.tag = 1
            UIView.animate(withDuration: 0.4, animations: {
                sender.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(M_PI)) / 180.0)
            })
            
            
            
            var page = scrollView.contentOffset.x / scrollView.frame.size.width;
            
            page+=1
            

            print("page number= \(page)")
            
            currentNumber = Int(page)
            
            //noteOnIndex = allContacts[currentNumber-1].notes.note
            
            
            let subView = self.scrollView.subviews[self.currentNumber-1] as! scrollViewItem
            //  print(sub.opaque)
            
            subView.isUserInteractionEnabled = true
            
            subView.topSpace.constant = -220
            
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
            
            
            
            //            dispatch_async(dispatch_get_main_queue(), {
            //                
            //                
            //                let subView = self.scrollView.subviews[self.currentNumber-1] as! scrollViewItem
            //                //  print(sub.opaque)
            //                
            //                subView.userInteractionEnabled = true
            //                
            //                subView.topSpace.constant = -220
            //                
            //                UIView.animateWithDuration(0.4){
            //                    self.view.layoutIfNeeded()
            //                }
            //            })
            
            
            
            
        }
        else
        {
            sender.tag = 0
            UIView.animate(withDuration: 0.4, animations: {
                sender.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(M_PI)) / 180.0)
            })
            
            
            
            var page = scrollView.contentOffset.x / scrollView.frame.size.width;
            
            page+=1
            
            
            
            print("page number= \(page)")
            
            currentNumber = Int(page)
            
            //noteOnIndex = allContacts[currentNumber-1].notes.note
            
            
            let subView = self.scrollView.subviews[self.currentNumber-1] as! scrollViewItem
            //  print(sub.opaque)
            
            subView.isUserInteractionEnabled = true
            
            subView.topSpace.constant = 0
            
            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            })
            
            
            //            dispatch_async(dispatch_get_main_queue(), {
            //                
            //                let subView = self.scrollView.subviews[self.currentNumber-1] as! scrollViewItem
            //                //  print(sub.opaque)
            //                
            //                subView.userInteractionEnabled = true
            //                
            //                subView.topSpace.constant = 0
            //                
            //                UIView.animateWithDuration(0.4){
            //                    self.view.layoutIfNeeded()
            //                }
            //
            //            })
            
            
            
        }
        
        
    }
    
    
    
    func editTapped(_ str:String)
    {
        
        let groupName = str
        
        if (1 == 1)
        {
            
            var page = scrollView.contentOffset.x / scrollView.frame.size.width;
            page+=1
            print("page number= \(page)")
            
            currentNumber = Int(page)
            
            
            //self.createGroup(self.id, name: groupName!, secretCode: secretCode as String)
            
            //http://jasontask.azurewebsites.net/api/index.php?command=addNotes&user_id=917093491659259&contact_id=62&note=hellowwor
            
            print("user-id:\(self.userid)")
            print("contact-id:\(self.allContacts[self.currentNumber-1].contact_id)")
            print("note:\(groupName)")
            
            
            
            GTToast.create("Updating Notes. Please wait...").show()
            var apiCall = "http://jasontask.azurewebsites.net/api/index.php?command=addNotes&user_id=\(self.userid!)&contact_id=\(self.allContacts[self.currentNumber-1].contact_id!)&note=\(groupName)"
            
            apiCall = apiCall.replacingOccurrences(of: " ", with: "%20")
            
            print(apiCall)
            
            Alamofire.request(apiCall)
                .responseString { response in
                    if (response.result.value != nil)
                    {
                        print("Response String: \(response.result.value)")
                        GTToast.create(response.result.value!).show()
                        //self.allContacts = []
                        
                        if (response.result.value!.range(of: "successfully") != nil)
                        {
                            //self.allContacts[self.currentNumber-1].notes.note = groupName!
                            self.allContacts = []
                            //                self.getContactsForGroup(self.groupID)
                            
                        }
                        
                        //self.getContactsForGroup(self.groupID)
                    }
            }
            
            
        }
        else
        {
            print("Enter note or press cancel")
        }
        
        
        
        
    }
    
    func myImageUploadRequest()
    {

        AppDelegate().showLoading(str: "Uploading Image...")
        
        // var name:String = nametext.text!
        let myUrl:URL!
        myUrl = URL(string: "http://jasontask.azurewebsites.net/api/changepicture.php")!;
        //let myUrl = NSURL(string: "https://lfk.azurewebsites.net/upload.php");
        
        var request = URLRequest(url:myUrl!);
        request.httpMethod = "POST";
        
        var page = scrollView.contentOffset.x / scrollView.frame.size.width;
        
        print("page number= \(page)")
        
        currentNumber = Int(page)
        
        
        let param = [
            "userid"  : "\(self.id!)",
            "contactid"    : "\(self.allContacts[currentNumber].contact_id!)",
        ]
        
        
        
        print("myUrl: \(myUrl) params: userid=\(self.id), contactid=\(self.allContacts[currentNumber].contact_id)");
        
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        let imageData = UIImageJPEGRepresentation(im, 0)
        
        if(imageData==nil)  { return; }
        
        request.httpBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData!, boundary: boundary)
        

        
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            data, response, error in

            AppDelegate().dissLoading()

            if error != nil {

                print("error=\(error)")

                AppDelegate().showMsg(str: "Error", success: false)
                return
            }
            else{
                AppDelegate().showMsg(str: "Contact created", success: true)

            }


            // You can print out response object
            print("******* response = \(response)")

            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")

            //self.performSegueWithIdentifier("contactAdded", sender: self)





            //self.dismiss(animated: true, completion: nil);

            //self.performSegueWithIdentifier("contactHasBeenAddedYes", sender: self)

            //unwindForSegue(<#T##unwindSegue: UIStoryboardSegue##UIStoryboardSegue#>, towardsViewController: <#T##UIViewController#>)

            //            do{
            //             let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableContainers) as? NSDictionary
            //                print(json)
            //            } catch {
            //
            //            }





            //            dispatch_async(dispatch_get_main_queue(),{
            //                self.myActivityIndicator.stopAnimating()
            //                    self.contactImage.image = nil;
            //            });

            /*
             if let parseJSON = json {
             var firstNameValue = parseJSON["firstName"] as? String
             println("firstNameValue: \(firstNameValue)")
             }
             */
            
        }) 
        
        task.resume()
        
    }

     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {

            im = image

            //self.uploadingDialog.isHidden = false

            //animator.startAnimating()


        } else{
            print("Something went wrong")
        }

        self.dismiss(animated: true, completion: nil)

        myImageUploadRequest()


    }



    @IBAction func optionButtonPressed(_ sender: UIBarButtonItem) {
        
        more(sender)
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print ("text here: \(textView.text!)")
        if textView.text! == "No Notes Added" {
            textView.text = nil
        }
    }
    
    
    
    @IBOutlet weak var animator: UIActivityIndicatorView!
    
    func setAllViews()
    {
        //self.scrollView.frame = CGRectMake(0,0, self.view.frame.width, self.view.frame.height)
        let navBarHeight = self.navigationController!.navigationBar.frame.size.height
        let ScrollViewHeight = self.scrollView.frame.height
        let ScrollViewWidth = self.scrollView.frame.width
        
        //        containerView = UIView()
        //        containerView.userInteractionEnabled = true
        
        
        //   self.scrollView.addSubview(containerView)
        self.automaticallyAdjustsScrollViewInsets = false

        
        
        for i in 0...self.count-1{
            
            //var view = scrollViewItem(frame: CGRectMake(ScrollViewWidth*CGFloat(i),0, ScrollViewWidth, ScrollViewHeight))
            
            
            var view = scrollViewItem.instanceFromNib()
            view.frame = CGRect(x: ScrollViewWidth*CGFloat(i),y: 0, width: ScrollViewWidth, height: ScrollViewHeight)

            Alamofire.request(allContacts[i].picture).responseImage { response in
                debugPrint(response)

                print(response.request)
                print(response.response)
                debugPrint(response.result)

                if let image = response.result.value {
                    view.logo.image = image
                }
            }

//            view.logo.image =
//                NSURL(string: allContacts[i].picture as! String)
//                    .flatMap { NSData(contentsOf: $0 as URL) }
//                    .flatMap { UIImage(data: $0 as Data) }


//            view.logo.loadImageFromURLString(allContacts[i].picture as! String, placeholderImage: UIImage(named: "loading")) {
//                (finished, error) in
//            }
//            
            view.isUserInteractionEnabled = true
            
            
            
            view.name.text = self.allContacts[i].name
            
            
            if (self.allContacts[i].notes != "Note has been added")
            {
                view.note.text = self.allContacts[i].notes
            }
            else
            {
                view.note.text = self.allContacts[i].notes
            }
            
            
            view.note.delegate = self;
            
            
            
            //            if (self.allContacts[i].notes.note.length>30)
            //            {
            //                // view.note.text = (self.allContacts[i].notes.note)
            //            }
            //            else
            //            {
            //                //view.note.text = (self.allContacts[i].notes.note)
            //                //view.note.text = (self.allContacts[i].notes.note).substringByInt(0, 28)
            //            }
            
            
            let singleTap = UIGestureRecognizer(target: self, action:#selector(TryViewController.createGp(_:)))
            singleTap.cancelsTouchesInView = false
            scrollView.addGestureRecognizer(singleTap)
            
            view.note.isUserInteractionEnabled = true
            
            view.note.addGestureRecognizer(singleTap)
            
            
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(TryViewController.createGp(_:)))
            view.isUserInteractionEnabled = true
            view.addGestureRecognizer(tapGestureRecognizer)
            
            
            print(i)
            self.scrollView.addSubview(view)
            self.scrollView.addGestureRecognizer(singleTap)
            
            
            
            
            self.scrollView.isUserInteractionEnabled = true
            
            
        }
        
        
        
        
        
        //let flt:CGFloat = 5.0 + self.scrollView.frame.width
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.width*CGFloat(self.count), height: self.scrollView.frame.height)
        
        
        
        
        
        scrollView.isUserInteractionEnabled = true
        
        scrollView.delaysContentTouches = false
        
        scrollView.canCancelContentTouches = false
        
        
    }
    
    
    func more(_ sender: UIBarButtonItem) {
        
        
        var page = scrollView.contentOffset.x / scrollView.frame.size.width;
        page+=1
        print("page number= \(page)")
        
        currentNumber = Int(page)
        
        
        let alertView = SCLAlertView()
        
        
        alertView.addButton("Add Contacts") {
            print("Add Contacts pressed")
            
            self.addContactsOptions()
            
        }
        
//        alertView.addButton("Cancel") {
//            print("Second button tapped")
//            alertView.dismiss(animated: true, completion: nil)
//        }
//        
//        



        
        //alertView.setCloseButton = false
        
        //   alertView.
        
        //        alertView.showSuccess("Options", subTitle: "Select your desired option.")
        
        alertView.showTitle("Options", // Title of view
            subTitle: "Select your desired option.", // String of view
            duration: 0.0, // Duration to show before closing automatically, default: 0.0
            completeText: nil, // Optional button value, default: ""
            style: .info, // Styles - see below.
            colorStyle: 0xE34D4E,
            colorTextButton: 0xFFFFFF
        )


        
        
        
        
        //        let alert = UIAlertController(title: "Options", message: "You can Manage Notes or Add Contacts.", preferredStyle: UIAlertControllerStyle.Alert)
        //        
        //        
        //        
        //        let notesOption = UIAlertAction(title: "Notes", style: UIAlertActionStyle.Default) {
        //            
        //            UIAlertAction in
        //            print("Notes")
        //            //self.editTapped()
        //            
        //        }
        //        let contactsOption = UIAlertAction(title: "Add Contact", style: UIAlertActionStyle.Default) {
        //            
        //            UIAlertAction in
        //            print("Contacts")
        //            self.addContactsOptions()
        //            
        //        }
        //        
        //        let cancelOption = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) {
        //            
        //            UIAlertAction in
        //            print("Cancel")
        //            alert.dismissViewControllerAnimated(true, completion: nil)
        //            
        //        }
        //        
        ////        if (allContacts.count>0)
        ////        {
        ////            alert.addAction(notesOption)
        ////        }
        //        
        //        alert.addAction(contactsOption)
        //        
        //        alert.addAction(cancelOption)
        //        
        //        self.presentViewController(alert, animated: true, completion: nil)
        //        
        //        

    }

    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {

        print ("swipe left")
    }

    @IBAction func rightSwipe(_ sender: UISwipeGestureRecognizer) {

        print ("right swipe")
    }

    @IBAction func down(_ sender: UISwipeGestureRecognizer) {

        let a = UIButton()
        a.tag = 1
        ViewNotesForPerson(a)

    }
    
    @IBAction func swipeGesture(_ sender: UISwipeGestureRecognizer) {
        
        let a = UIButton()
        a.tag = 0
        ViewNotesForPerson(a)
        
    }
    
    func addContactsOptions() {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        //self.navigationController?.popViewControllerAnimated(true)
        print("Button tapped")
        
        
        
        let alertView = SCLAlertView()
        
        
        alertView.addButton("Address Book") {
            print("address book")
            self.performSegue(withIdentifier: "address", sender: self)
            
        }
        
        alertView.addButton("New Contact") {
            print("new Contact")
            self.performSegue(withIdentifier: "newContact", sender: self)
        }
        
        /*alertView.addButton("Cancel") {
            print("Cancel")
            alertView.dismiss(animated: true, completion: nil)
        }*/
        
        
        
//        alertView. = false

        alertView.showTitle("Add Contacts", // Title of view
            subTitle: "You can add contacts by two ways.", // String of view
            duration: 0.0, // Duration to show before closing automatically, default: 0.0
            completeText: nil, // Optional button value, default: ""
            style: .info, // Styles - see below.
            colorStyle: 0xE34D4E,
            colorTextButton: 0xFFFFFF
        )
        
        
        //        let alert = UIAlertController(title: "Add Contact", message: "You can add contacts by two ways.", preferredStyle: UIAlertControllerStyle.Alert)
        //        
        //        
        //        
        //        let address = UIAlertAction(title: "Address Book", style: UIAlertActionStyle.Default) {
        //            
        //            UIAlertAction in
        //            print("address book")
        //            
        //            self.performSegueWithIdentifier("address", sender: self)
        //        }
        //        
        //        let newCon = UIAlertAction(title: "New Contact", style: UIAlertActionStyle.Cancel) {
        //            
        //            UIAlertAction in 
        //            print("new Contact")
        //            self.performSegueWithIdentifier("newContact", sender: self)
        //            
        //            
        //        }
        //        
        //        let cancelOption = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) {
        //            
        //            UIAlertAction in
        //            print("Cancel")
        //            alert.dismissViewControllerAnimated(true, completion: nil)
        //            
        //        }
        //        
        //        
        //        alert.addAction(cancelOption)
        //        alert.addAction(address)
        //        alert.addAction(newCon)
        //        
        //        
        //        self.presentViewController(alert, animated: true, completion: nil)
        //        
        
        
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        //        self.getContactsForGroup(self.groupID)
        //        
        //        
        if let id: AnyObject = UserDefaults.standard.object(forKey: "id") as AnyObject? {
            self.id = id as! String
            
            
        }
        else
        {
            print("not set")
        }
    }
    
    @IBAction func unwindToVC(_ segue: UIStoryboardSegue) {
        
        print("it came here....")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        scrollView.panGestureRecognizer.delaysTouchesBegan = false;

        scrollView.delegate = self
        
        if let id: AnyObject = UserDefaults.standard.object(forKey: "id") as AnyObject? {
            self.userid = id as! String
            
        }
        else
        {
            print("not set")
        }
        
        self.title = "Contacts"
        
        //        var More : UIBarButtonItem = UIBarButtonItem(title: "...", style: UIBarButtonItemStyle.Plain, target: self, action: "more:")
        //        //More.image = UIImage(named: "btn1")
        //        
        //        More.tintColor = UIColor.whiteColor()
        //        self.navigationItem.rightBarButtonItem = More
        
        
        
        if let id: AnyObject = UserDefaults.standard.object(forKey: "currentGpID") as AnyObject? {
            self.groupID = id as! String
            
        }
        
        
        
        
        print(self.groupID)
        getContactsForGroup(self.groupID)
        
        
        
        //        view0.view = UIView.loadFromNibNamed("scrollViewItem")
        //        view1.view = UIView.loadFromNibNamed("scrollViewItem")
        //        view2.view = UIView.loadFromNibNamed("scrollViewItem")
        //        view3.view = UIView.loadFromNibNamed("scrollViewItem")
        //        view4.view = UIView.loadFromNibNamed("scrollViewItem")
        //        view5.view = UIView.loadFromNibNamed("scrollViewItem")
        
        
        
        // Keyboard stuff.
        let center: NotificationCenter = NotificationCenter.default
        center.addObserver(self, selector: #selector(TryViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(TryViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func keyboardWillShow(_ notification: Notification) {
        let info:NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        
        let keyboardHeight: CGFloat = keyboardSize.height
        
        let _: CGFloat = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber as CGFloat
        
        
        UIView.animate(withDuration: 0.25, delay: 0.25, options: UIViewAnimationOptions(), animations: {
            self.view.frame = CGRect(x: 0, y: (self.view.frame.origin.y - keyboardHeight + 50), width: self.view.bounds.width, height: self.view.bounds.height)
            }, completion: nil)
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        let info: NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        
        let keyboardHeight: CGFloat = keyboardSize.height
        
        let _: CGFloat = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber as CGFloat
        
        UIView.animate(withDuration: 0.25, delay: 0.25, options: UIViewAnimationOptions(), animations: {
            self.view.frame = CGRect(x: 0, y: (self.view.frame.origin.y + keyboardHeight - 50), width: self.view.bounds.width, height: self.view.bounds.height)
            }, completion: nil)
        
    }

    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func getContactsForGroup(_ id: String)
    {
        if (scrollView != nil)
        {
            scrollView.subviews.map({ $0.removeFromSuperview() })
        }
        
        
        
        
        allContacts = []
        GTToast.create("Loading contacts. Please wait...").show()
        var apiCall = "http://jasontask.azurewebsites.net/api/index.php?command=loadGroupPeople&user_id=\(self.userid!)&group_id=\(id)"
        
        print(apiCall)
        
        Alamofire.request(apiCall)
            .responseJSON { response in
                
                let j  = response.result.value as? NSDictionary

                if (j != nil)
                {
                    var json = j!

                    let result = json["result"] as! [[String : AnyObject]]

                    self.allContacts = []

                    if (!result.isEmpty)
                    {
                        for r in result {

                            let id = r["contact_id"]! as! String
                            let by = r["added_by"]! as! String
                            let gp = r["group_id"]! as! String
                            let name = r["name"]! as! String
                            let picture = r["picture"] as! String
                            let notes = r["notes"] as! String //[[String:String]]

                            var allNotes:[[String:String]]
                            allNotes = []



                            var c:contact!
                            c = contact(id:id, by:by, gp:gp, name:name, picture:picture, added:" ", notes:notes)
                            // }

                            self.allContacts.append(c)

                        }

                        print(self.allContacts.count)
                        self.count = self.allContacts.count
                        self.setAllViews()


                        self.animator.stopAnimating()
                        self.uploadingDialog.isHidden = true
                        //                    DispatchQueue.main.asynchronously(execute: {
                        //
                        //                        self.animator.stopAnimating()
                        //                        self.uploadingDialog.isHidden = true
                        //
                        //                    })



                        //                    self.tableView.reloadData()
                        //self.performSegueWithIdentifier("joinedGroups", sender: self)
                        
                    }
                    else
                    {
                        self.viewNotesBtn.isHidden = true
                        self.imgChange.isHidden = true
                        GTToast.create("No members in this group.").show()
                    }


                }
                else{
                    GTToast.create("No Members for this group").show()
                }


        }
        
        
    }
    
    
    
    class contact
    {
        
        var contact_id:String!
        var added_by:String!
        var group_id:String!
        var name:String!
        var picture:String!
        var date_added:String!
        var notes:String!
        
        
        
        
        init(id:String, by:String, gp:String, name:String, picture:String, added:String, notes:String)
        {
            self.contact_id = id
            self.added_by = by
            self.group_id = gp
            self.name = name
            self.picture = picture
            self.date_added = added
            self.notes = notes
        }
        
        
    }
    
    
    
}



