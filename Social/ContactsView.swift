//
//  ContactsView.swift
//  Social
//
//  Created by Aqib on 30/12/2016.
//  Copyright © 2016 root-dev. All rights reserved.
//

import GTToast
import Alamofire
import Foundation
import SCLAlertView
import AlamofireImage

import UIKit

class ContactsView: UIViewController, UIScrollViewDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imageBtn: UIButton!
    var im:UIImage!
	@IBOutlet weak var moveUpBtn: UIButton!
    @IBOutlet weak var nameTopConst: NSLayoutConstraint!
    @IBOutlet weak var note: UITextView!
    @IBOutlet weak var contactName: UILabel!
    var allContacts: [contact] = []
    var userid:String!
    @IBOutlet weak var scrollView: UIScrollView!
	var currentItemNumber = 0
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		getContactsForGroup(self.userid)

		self.scrollView.delegate = self



		let center: NotificationCenter = NotificationCenter.default
		center.addObserver(self, selector: #selector(ViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		center.addObserver(self, selector: #selector(ViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

		let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
		view.addGestureRecognizer(tap)

		self.note.delegate = self
    }

	override func viewDidAppear(_ animated: Bool) {

		if let a = UserDefaults.standard.value(forKey: "contactAdded"){

			if (a as! Bool)
			{
				self.getContactsForGroup(self.userid)
			}
		}

	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

		if (segue.identifier == "address")
		{


			let destinationNavigationController = segue.destination as! UINavigationController


			let destination = destinationNavigationController.topViewController as! NewContact
			destination.open = true

			// self.performSegue(withIdentifier: "addressB", sender: self)

		}

	}

	
	func dismissKeyboard() {
		//Causes the view (or one of its embedded text fields) to resign the first responder status.
		view.endEditing(true)
		editTapped(self.note.text!)
	}



    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }


	func keyboardWillHide(_ sender: Notification) {


		if (self.moveUpBtn.tag == 0)
		{


			if let keyboardSize = (sender.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
				self.view.frame.origin.y += keyboardSize.height
			}
		}
		
	}

	func keyboardWillShow(_ sender: Notification) {


		if (self.moveUpBtn.tag == 0)
		{

			if let keyboardSize = (sender.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
				self.view.frame.origin.y -= keyboardSize.height
			}


		}

	}


    func randomStringWithLength(length: Int) -> String {

        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)

        var randomString = ""

        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }

        return randomString
    }
    
    func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        let body = NSMutableData();

        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }


        let filename = "user-image-\(randomStringWithLength(length: 15)).jpg"

        let mimetype = "image/jpg"

        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")



        body.appendString("--\(boundary)--\r\n")

        return body as Data
    }

    @IBAction func actions(_ sender: UIBarButtonItem) {


        let alertView = SCLAlertView()


        alertView.addButton("Add Contacts") {
            print("Add Contacts pressed")

            self.addContactsOptions()

        }

        alertView.showTitle("Options", // Title of view
            subTitle: "Select your desired option.", // String of view
            duration: 0.0, // Duration to show before closing automatically, default: 0.0
            completeText: nil, // Optional button value, default: ""
            style: .info, // Styles - see below.
            colorStyle: 0xE34D4E,
            colorTextButton: 0xFFFFFF
        )

    }

    func addContactsOptions() {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        //self.navigationController?.popViewControllerAnimated(true)
        print("Button tapped")



        let alertView = SCLAlertView()


        alertView.addButton("Address Book") {
            print("address book")
            self.performSegue(withIdentifier: "address", sender: self)

        }

        alertView.addButton("New Contact") {
            print("new Contact")
            self.performSegue(withIdentifier: "newContact", sender: self)
        }




        //        alertView. = false

        alertView.showTitle("Add Contacts", // Title of view
            subTitle: "You can add contacts by two ways.", // String of view
            duration: 0.0, // Duration to show before closing automatically, default: 0.0
            completeText: nil, // Optional button value, default: ""
            style: .info, // Styles - see below.
            colorStyle: 0xE34D4E,
            colorTextButton: 0xFFFFFF
        )








    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        if let image = info[UIImagePickerControllerEditedImage] as? UIImage {

            im = image



        } else{
            print("Something went wrong")
        }

        self.dismiss(animated: true, completion: nil)
        
        myImageUploadRequest()
        
        
    }

    func myImageUploadRequest()
    {

        AppDelegate().showLoading(str: "Uploading Image...")

        // var name:String = nametext.text!
        let myUrl:URL!
        myUrl = URL(string: "http://jasontask.azurewebsites.net/api/changepicture.php")!;
        //let myUrl = NSURL(string: "https://lfk.azurewebsites.net/upload.php");

        var request = URLRequest(url:myUrl!);
        request.httpMethod = "POST";

        let param = [
            "userid"  : "\(self.userid!)",
            "contactid"    : "\(self.allContacts[currentItemNumber].contact_id!)",
        ]


		//print(myUrl)
        print("myUrl: \(myUrl!) params: userid=\(self.allContacts[self.currentItemNumber].contact_id!), contactid=\(self.allContacts[currentItemNumber].contact_id!)");


        let boundary = generateBoundaryString()

        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")


        let imageData = UIImageJPEGRepresentation(im, 0)

        if(imageData==nil)  { return; }

        request.httpBody = createBodyWithParameters(param, filePathKey: "file", imageDataKey: imageData!, boundary: boundary)



        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            data, response, error in

            AppDelegate().dissLoading()

            if error != nil {

                print("error=\(error)")

                AppDelegate().showMsg(str: "Error", success: false)
                return
            }
            else{
                AppDelegate().showMsg(str: "Contact created", success: true)

            }


            // You can print out response object
            print("******* response = \(response)")

            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")

			self.getContactsForGroup(self.userid)

            
            
        }) 
        
        task.resume()
        
    }

	@IBAction func imageChangeTapped(_ sender: UIButton) {

        print("Image Tapped")
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            print("Button capture")


            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = true

            self.present(imagePicker, animated: true, completion: nil)
        }


	}



	func editTapped(_ str:String)
	{

		let groupName = str

		if (1 == 1)
		{

			var page = scrollView.contentOffset.x / scrollView.frame.size.width;
			page+=1
			print("page number= \(page)")

			//currentNumber = self.currentItemNumber


			//self.createGroup(self.id, name: groupName!, secretCode: secretCode as String)

			//http://jasontask.azurewebsites.net/api/index.php?command=addNotes&user_id=917093491659259&contact_id=62&note=hellowwor

			print("user-id:\(self.userid)")
			print("contact-id:\(self.allContacts[self.currentItemNumber].contact_id)")
			print("note:\(groupName)")



			GTToast.create("Updating Notes. Please wait...").show()
			var apiCall = "http://jasontask.azurewebsites.net/api/index.php?command=addNotes&user_id=\(self.userid!)&contact_id=\(self.allContacts[self.currentItemNumber].contact_id!)&note=\(groupName)"

			apiCall = apiCall.replacingOccurrences(of: " ", with: "%20")

			print(apiCall)

			Alamofire.request(apiCall)
				.responseString { response in
					if (response.result.value != nil)
					{
						print("Response String: \(response.result.value)")
						GTToast.create(response.result.value!).show()
						//self.allContacts = []

						if (response.result.value!.range(of: "successfully") != nil)
						{
							//self.allContacts[self.currentNumber-1].notes.note = groupName!
							self.allContacts = []
							//                self.getContactsForGroup(self.groupID)

						}

						//self.getContactsForGroup(self.groupID)
					}
			}


		}
		else
		{
			print("Enter note or press cancel")
		}




	}


	func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

		if(text == "\n") {
			textView.resignFirstResponder();


			editTapped(textView.text!)



			return false;
		}

		return true;
	}

	func setViewsForContact(n:Int)
	{

		self.contactName.text = self.allContacts[n].name
		self.note.text = self.allContacts[n].notes

		UIView.animate(withDuration: 0.5, animations: {
			self.contactName.alpha = 1.0
			self.note.alpha = 1.0
            self.imageBtn.alpha = 1.0
		})
	}

	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {



		print ("Scrolling ended at: \(scrollView.contentOffset.x/scrollView.frame.width)")
		self.currentItemNumber = Int(scrollView.contentOffset.x/scrollView.frame.width)

		setViewsForContact(n:self.currentItemNumber)

	}

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		UIView.animate(withDuration: 0.5, animations: {
			self.contactName.alpha = 0
			self.note.alpha = 0
		})
	}



	func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//		self.contactName.isHidden = false

	}


	@IBAction func move(_ sender: UIButton) {

        self.view.layoutIfNeeded()
        sender.layoutIfNeeded()

        if (sender.tag == 0)
        {
            UIView.animate(withDuration: 1, animations: { () -> Void in
                self.nameTopConst.constant = -200
                sender.tag = -1
            })

            }
        else{
            UIView.animate(withDuration: 1, animations: { () -> Void in

                self.nameTopConst.constant = 0
                sender.tag = 0
            })
        }




	}


    func getContactsForGroup(_ id: String)
    {
        if (scrollView != nil)
        {
            scrollView.subviews.map({ $0.removeFromSuperview() })
        }




        allContacts = []
        GTToast.create("Loading contacts. Please wait...").show()
        var apiCall = "http://jasontask.azurewebsites.net/api/index.php?command=loadGroupPeople&user_id=\(self.userid!)&group_id=\(id)"

        print(apiCall)

        Alamofire.request(apiCall)
            .responseJSON { response in

                let j  = response.result.value as? NSDictionary

                if (j != nil)
                {
                    var json = j!

                    let result = json["result"] as! [[String : AnyObject]]

                    self.allContacts = []

                    if (!result.isEmpty)
                    {
                        for r in result {

                            let id = r["contact_id"]! as! String
                            let by = r["added_by"]! as! String
                            let gp = r["group_id"]! as! String
                            let name = r["name"]! as! String
                            let picture = r["picture"] as! String
                            let notes = r["notes"] as! String //[[String:String]]

                            var allNotes:[[String:String]]
                            allNotes = []



                            var c:contact!
                            c = contact(id:id, by:by, gp:gp, name:name, picture:picture, added:" ", notes:notes)
                            // }

                            self.allContacts.append(c)

                        }

                        print("\(self.allContacts.count) contacts loaded.")

                        self.setupView()
						self.setViewsForContact(n:0)



                    }
                    else
                    {
                        GTToast.create("No members in this group.").show()
                    }
                    
                    
                }
                else{
                    GTToast.create("No Members for this group").show()
                }
                
                
        }
        
        
    }


    func heightOfNavBar() -> CGFloat
    {

        return 0.0//self.navigationController!.navigationBar.frame.height
    }

	var ivWidthConstraint:NSLayoutConstraint!
	var ivHeightConstraint:NSLayoutConstraint!


    func createImageViewForContact(n:CGFloat, scW:CGFloat, scH:CGFloat) -> UIImageView
    {
        var imgV = UIImageView(frame: CGRect(x: scW*(n), y: heightOfNavBar(), width: scW, height: scH))
        imgV.image = UIImage(named: "logo")

        imgV.contentMode = .scaleAspectFill

        Alamofire.request(self.allContacts[Int(n)].picture).responseImage { response in
            debugPrint(response)

            print(response.request)
            print(response.response)
            debugPrint(response.result)

            if let image = response.result.value {
                imgV.image = image
            }
        }

		imgV.clipsToBounds = true

        return imgV
    }


    func createTextBoxBGForContact(n:CGFloat, scW:CGFloat, scH:CGFloat) -> UIImageView
    {
        var bgImageView = UIImageView(frame: CGRect(x: scW*(n), y: 2*scH/3+heightOfNavBar(), width: scW, height: scH/3))
        bgImageView.image = UIImage(named: "bg-note")

        return bgImageView
    }

    func createNotesHeadingForContact(n:CGFloat, scW:CGFloat, scH:CGFloat) -> UILabel
    {
        var lbl = UILabel(frame: CGRect(x: scW*(n), y: 2*scH/3+heightOfNavBar(), width: scW, height: 30))

        lbl.text = self.allContacts[Int(n)].name.capitalized
        lbl.textAlignment = .center

        lbl.textColor = UIColor.white

        return lbl
    }

    func createTextBoxForContact(n:CGFloat, scW:CGFloat, scH:CGFloat) -> UITextView
    {
        var textV = UITextView(frame: CGRect(x: scW*(n), y: 2*scH/3+heightOfNavBar()+30, width: scW, height: scH/3-30))
        textV.text = self.allContacts[Int(n)].notes
        textV.backgroundColor = UIColor.clear
        textV.textColor = UIColor.white
        return textV
    }



    func setupView()
    {


        var ScrollViewWidth = self.view.frame.width
        var ScrollViewHeight = self.scrollView.frame.height

        var ScrollViewContentWidth = ScrollViewWidth*CGFloat(self.allContacts.count)

		self.scrollView.contentSize = CGSize(width: ScrollViewWidth*CGFloat(self.allContacts.count), height: ScrollViewHeight)


		self.scrollView.translatesAutoresizingMaskIntoConstraints = false


        for n in 1...self.allContacts.count{

			var iv = createImageViewForContact(n: CGFloat(n-1), scW: ScrollViewWidth, scH: ScrollViewHeight)
            iv.tag = 3000
			self.scrollView.addSubview(iv)
            
//			var bg = createTextBoxBGForContact(n: CGFloat(n-1), scW: ScrollViewWidth, scH: ScrollViewHeight)
//            bg.tag = 2000
//
//            var heading = createNotesHeadingForContact(n: CGFloat(n-1), scW: ScrollViewWidth, scH: ScrollViewHeight)
//			heading.tag = 1000
//
//
//			var textBlock = createTextBoxForContact(n: CGFloat(n-1), scW: ScrollViewWidth, scH: ScrollViewHeight)
//            textBlock.tag = 4000
//
//			var headingTopConstraint = NSLayoutConstraint(item: heading, attribute: .topMargin, relatedBy: .equal, toItem: iv, attribute: .bottom, multiplier: 1, constant: 0)
//
//			var headingWidthConstraint = NSLayoutConstraint(item: heading, attribute: .width, relatedBy: .equal, toItem: self.scrollView, attribute: .width, multiplier: 1, constant: 0)
//
//			heading.setNeedsLayout()
//			heading.translatesAutoresizingMaskIntoConstraints = false
//
//
////			var topConstraint = NSLayoutConstraint(item: bg, attribute: .top, relatedBy: .equal, toItem: heading, attribute: .top, multiplier: 1, constant: 0)
////
////			var	bottomConstraint = NSLayoutConstraint(item: bg, attribute: .bottom, relatedBy: .equal, toItem: textBlock, attribute: .bottom, multiplier: 1, constant: 0)
////
////			var	marginConstraint = NSLayoutConstraint(item: textBlock, attribute: .top, relatedBy: .equal, toItem: heading, attribute: .bottom, multiplier: 1, constant: 0)
////
////			var	IVMarginConstraint = NSLayoutConstraint(item: iv, attribute: .leading, relatedBy: .equal, toItem: self.scrollView, attribute: .leadingMargin, multiplier: 1, constant: self.view.frame.width*CGFloat(n))
//
//			//var	marginConstraint = NSLayoutConstraint(item: textBlock, attribute: .top, relatedBy: .equal, toItem: heading, attribute: .bottom, multiplier: 1, constant: 0)
//			
//
//			//ivWidthConstraint = NSLayoutConstraint(item: iv, attribute: .width, relatedBy: .equal, toItem: self.scrollView, attribute: .width, multiplier: 1, constant: 0)
//
//			//ivHeightConstraint = NSLayoutConstraint(item: iv, attribute: .height, relatedBy: .equal, toItem: self.scrollView, attribute: .height, multiplier: 0.6, constant: 0)
//
//
//
////			iv.setNeedsLayout()
////			iv.translatesAutoresizingMaskIntoConstraints = false
////
////			bg.setNeedsLayout()
////			bg.translatesAutoresizingMaskIntoConstraints = false
////
////			heading.setNeedsLayout()
////			heading.translatesAutoresizingMaskIntoConstraints = false
////
////			textBlock.setNeedsLayout()
////			textBlock.translatesAutoresizingMaskIntoConstraints = false
////
//
//
//
//            //self.scrollView.addSubview(bg)
//
//            //self.scrollView.addSubview(heading)
//
//            //self.scrollView.addSubview(textBlock)
//
//			//self.scrollView.addConstraints([headingTopConstraint,headingWidthConstraint])
//			//self.scrollView.addConstraints([topConstraint, bottomConstraint, marginConstraint, ivWidthConstraint, ivHeightConstraint])
//
//			//self.scrollView.setNeedsLayout()
//
//
////            bg.topAnchor.constraint(equalTo: heading.topAnchor).isActive = true
////
////            bg.bottomAnchor.constraint(equalTo: textBlock.bottomAnchor).isActive = true
////
////            textBlock.topAnchor.constraint(equalTo: heading.bottomAnchor).isActive = true
//
//
//
//
//			//self.scrollView.com.addConstraint(topConstraint)
//
//			//self.scrollView.addConstraint(bottomConstraint)
//
//			//self.scrollView.addConstraint(marginConstraint)

			

        }

    }

    class contact
    {

        var contact_id:String!
        var added_by:String!
        var group_id:String!
        var name:String!
        var picture:String!
        var date_added:String!
        var notes:String!




        init(id:String, by:String, gp:String, name:String, picture:String, added:String, notes:String)
        {
            self.contact_id = id
            self.added_by = by
            self.group_id = gp
            self.name = name
            self.picture = picture
            self.date_added = added
            self.notes = notes
        }


    }

}
