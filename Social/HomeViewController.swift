//
//  HomeViewController.swift
//  Social
//
//  Created by Aqib on 04/10/2015.
//  Copyright © 2015 Root. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Alamofire
import AlamofireImage

class HomeViewController: UIViewController {

    
    var id:String = "id"
    var name:String = "username"
    var email:String = "email"
    var about:String = "bio"
    var picture:String = "picture"
    var signup_method = "method"
    
    
    @IBOutlet weak var loggedin: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath:  "me", parameters:["fields": "id, name, email, bio, picture"])
        
        graphRequest.start(completionHandler: { (connection, res, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error?.localizedDescription)")
            }
            else
            {
                var result = res as! NSDictionary
                self.id = (result.value(forKey: "id") as? String)!
                self.name = (result.value(forKey: "name") as? String)!
                self.email = (result.value(forKey: "email") as? String)!
                self.about = (result.value(forKey: "bio") as? String)!
                let n = (result.value(forKey: "picture") as? NSDictionary)!
                let m = (n.value(forKey: "data") as? NSDictionary)!
                self.picture = (m.value(forKey: "url") as? String)!
                self.signup_method = "facebook"
                
                print(self.id)
                print(self.name)
                print(self.email)
                print(self.about)
                print(self.picture)
                print(self.signup_method)
                
                var url = "https://jasontask.azurewebsites.net/api/index.php?command=add_social_user&"
                url += "registrar_id="
                url += self.id
                url += "&username="
                url += self.name
                url += "&profile_picture="
                url += self.picture
                url += "&signup_method="
                url += self.signup_method
                url += "&user_about="
                url += self.about
                print("----")
                url = url.replacingOccurrences(of: " ", with: "%20")
                print(url)
                print("----")
                var checkUrl = "https://jasontask.azurewebsites.net/api/index.php?command=check_registrar&registrar_id="
                checkUrl += self.id
                checkUrl = checkUrl.replacingOccurrences(of: " ", with: "%20")
                print(checkUrl)
                
                
                
                Alamofire.request(checkUrl, method:.get )
                    .responseJSON { response in
                        print(response.request)  // original URL request
                        print(response.response) // URL response
                        print(response.data)     // server data
                        print(response.result)   // result of response serialization
                        
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                        }
                }
                
                
//                Alamofire.request(.GET, checkUrl, parameters: ["foo": "bar"])
//                    .responseJSON { response in
//                        print(response.request)  // original URL request
//                        print(response.response) // URL response
//                        print(response.data)     // server data
//                        print(response.result)   // result of response serialization
//                        
//                        if let JSON = response.result.value {
//                            print("JSON: \(JSON)")
//                        }
//                }
                
                
                self.loggedin.text = "Logged in as " + self.name
                
            }
        })

        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
