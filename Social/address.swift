//
//  address.swift
//  FaceFlip
//
//  Created by Aqib on 14/11/2015.
//  Copyright © 2015 CodeArray. All rights reserved.
//

import UIKit
import Contacts
import GTToast

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

class address: UITableViewController, UISearchResultsUpdating {

    var nnn = 0;

    var resultSearchController = UISearchController()
    
    var contacts:[String] = []

    var filteredContacts:[String] = []
    var allContactsArray:NSArray = []
    var selection:Int = 0

    var SelectedString = ""

    let contactStore = CNContactStore()

    var contactsA:[CNContact]!

    var sections : [(index: Int, length :Int, title: String)] = Array()

    var contactImages:[UIImage] = []

    var sectionedArray:[Character: [String]]!



    var imgAvl:[Bool] = []

    var iii:UIImage!


    var results: [CNContact] = []
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {

        if segue.identifier == "selectedContact"{

            let vc = segue.destination as! NewContact
            vc.contactName = SelectedString
            vc.iii = self.iii

        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func getContactsFromAddressbook() -> [CNContact]
    {

        self.results = []

        do {
            try self.contactStore.enumerateContacts(with: CNContactFetchRequest(keysToFetch: [CNContactGivenNameKey as CNKeyDescriptor, CNContactFamilyNameKey as CNKeyDescriptor, CNContactMiddleNameKey as CNKeyDescriptor, CNContactEmailAddressesKey as CNKeyDescriptor,CNContactPhoneNumbersKey as CNKeyDescriptor, CNContactImageDataKey as CNKeyDescriptor, CNContactImageDataAvailableKey as CNKeyDescriptor])) {
                (contact, cursor) -> Void in
                self.results.append(contact)


            }
        }
        catch{
            print("Handle the error please")
        }

        print ("Contacts Size: \(self.results.count)")

        self.contacts = []

        self.nnn = 0

        for x in self.results
        {


            //self.contacts.append(CNContactFormatter.string(from: x, style: .fullName)!)

            var nn = ""

            if (x.givenName.characters.count != 0)
            {
                nn = "\(x.givenName) "
            }

            if (x.middleName.characters.count != 0)
            {
                nn = "\(nn)\(x.middleName) "
            }

            if (x.familyName.characters.count != 0)
            {
                nn = "\(nn)\(x.familyName)"
            }


            nn = "\(nn)|\(self.nnn)"

            self.nnn += 1

            nn.trimmingCharacters(in: .whitespacesAndNewlines)


            if (nn.characters.count > 2)
            {
                self.contacts.append(nn.capitalizingFirstLetter())
//                self.contactImages.append(x.imageData)

                self.imgAvl.append(x.imageDataAvailable)

                if (x.imageDataAvailable)
                {
                    self.contactImages.append(UIImage(data: x.imageData!)!)

                }else{
                        self.contactImages.append(UIImage(named: "logo")!)
                }
            }


        }

        self.tableView.reloadData()


        contacts.sort { $0 < $1 }


        var index = 0;

        for  i in 0 ..< contacts.count {

            let commonPrefix = contacts[i].commonPrefix(with: contacts[index], options: .caseInsensitive)

            if ((commonPrefix.characters.count) == 0 ) {
                
                let string = contacts[index].uppercased();
                
                let firstCharacter = string[string.startIndex]
                
                let title = "\(firstCharacter)"
//
//                let image:UIImage!
//
//
//                var fullNameArr = SelectedString.components(separatedBy: "|")
                
//                
//
//
//                if (self.imgAvl[selectedNum])
//                {
//                    image = UIImage(data: self.results[selectedNum].imageData!)
//                }
//                else{
//                    image = UIImage(named: "logo.png")!
//                }

                let newSection = (index: index, length: i - index, title: title)
                
                sections.append(newSection)
                
                index = i;
                
            }
            
        }

        return results


        
        
    }


    @IBOutlet var searchController: UISearchDisplayController!

    
    override func viewDidAppear(_ animated: Bool)
    {
       
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()



//        // Build letters array:
//
//        var letters: [Character]
//
//        letters = contacts.map { (name) -> Character in
//            return name[name.startIndex]
//        }
//
//        letters = letters.sorted()
//
//        letters = letters.reduce([], { (list, name) -> [Character] in
//            if !list.contains(name) {
//                return list + [name]
//            }
//            return list
//        })
//
//
//        // Build contacts array:
//
//        self.sectionedArray = [Character: [String]]()
//
//        for entry in contacts {
//
//            if sectionedArray[entry[entry.startIndex]] == nil {
//                sectionedArray[entry[entry.startIndex]] = [String]()
//            }
//            
//            sectionedArray[entry[entry.startIndex]]!.append(entry)
//            
//        }
//        
//        for (letter, list) in sectionedArray {
//            sectionedArray[letter] = list.sorted()
//        }
//        //


        self.resultSearchController = UISearchController(searchResultsController: nil)
        self.resultSearchController.searchResultsUpdater = self
        self.resultSearchController.dimsBackgroundDuringPresentation = false
        self.resultSearchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = self.resultSearchController.searchBar

        self.tableView.reloadData()



        let status : CNAuthorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)

        let contactStore = CNContactStore()


        if status == CNAuthorizationStatus.notDetermined{

            contactStore.requestAccess(for: .contacts, completionHandler: { (bool, error) in

//                if bool {
//                    //call contacts fetching function
//
//                    self.contactsA = self.getContactsFromAddressbook()
//
//                } else {
//                    
//                    GTToast.create("Give Conatcts Permission to App First").show()
//                }

            })

        }

//
//            contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (temp: Bool,  error : NSError?) -> Void in
//                //call contacts fetching function
//
//                self.contactsA = self.getContactsFromAddressbook()
//
//            } as! (Bool, Error?) -> Void)
//
//        }

        else if status == CNAuthorizationStatus.authorized {
            //call contacts fetching function

            self.contactsA = self.getContactsFromAddressbook()

        }

        else if status == CNAuthorizationStatus.denied {

            GTToast.create("Give Conatcts Permission to App First").show()
        }


        //getAllContacts()
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        //selection = (indexPath as NSIndexPath).row;

//        SelectedString = sections[indexPath]
//        print(selection)
//        self.performSegue(withIdentifier: "selectedContact", sender: self)



        if (self.resultSearchController.isActive)
        {

            SelectedString = filteredContacts[(indexPath as NSIndexPath).row] as! String
            print(filteredContacts[(indexPath as NSIndexPath).row])
            self.resultSearchController.isActive = false

            var fullNameArr = SelectedString.components(separatedBy: "|")

            var selectedNum = Int(fullNameArr.last!)!

            if (self.imgAvl[selectedNum])
            {
                self.iii = contactImages[selectedNum]
            }else{
                self.iii = UIImage(named: "logo")!
            }

            SelectedString = fullNameArr.first!

        }
        else{

            SelectedString = contacts[sections[indexPath.section].index + indexPath.row]

            var fullNameArr = SelectedString.components(separatedBy: "|")

            var selectedNum = Int(fullNameArr.last!)!

            self.iii = self.contactImages[selectedNum]

            SelectedString = fullNameArr.first!
        }


        self.performSegue(withIdentifier: "selectedContact", sender: self)

    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections

        if (self.resultSearchController.isActive)
        {
            return 1
        }else{
           return sections.count
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows

        if (self.resultSearchController.isActive)
        {
                return self.filteredContacts.count
        }
        else{

            return sections[section].length
        }

    }

    override func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {

        return index
    }

    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {

        var ind:[String] = []
        for x in sections{
            ind.append(x.title)
            print ("Sec: \(x.title)")
        }

        return ind;
        //return sections.map { $0.title }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        //        cell.textLabel?.text = contacts[indexPath.row]

        if (self.resultSearchController.isActive)
        {


            var fullNameArr = (filteredContacts[(indexPath as NSIndexPath).row] as! String).components(separatedBy: "|")



            cell.textLabel?.text = fullNameArr.first!

            print(filteredContacts[(indexPath as NSIndexPath).row])
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator


            return cell

        }
        else{

            var fullNameArr = (contacts[sections[indexPath.section].index + indexPath.row]).components(separatedBy: "|")

            cell.textLabel?.text = fullNameArr.first!

            //cell.textLabel?.text = contacts[(indexPath as NSIndexPath).row] as? String
              //cell.textLabel?.text =  fullNameArr.first           //print(contacts[(indexPath as NSIndexPath).row])
            //cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator


            return cell
        }

    }


    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        return sections[section].title
    }

    func updateSearchResults(for searchController: UISearchController) {

        self.filteredContacts.removeAll(keepingCapacity: false)
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)

        let array = (self.contacts as NSArray).filtered(using: searchPredicate)

        self.filteredContacts = array as! [String]

        self.tableView.reloadData()

    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }    
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
